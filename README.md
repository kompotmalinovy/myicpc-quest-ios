# MyICPC Quest iOS client

This is an iOS version of MyICPC Quest written in Swift using MVVM architecture and FRP approach.

---

## Features

App is fully(*) functional quest client with support for challenge/timeline submission and displaying data received from server nicely.
Context acquisition is still under development, Apple's security requirements differ from version to version, currently i'm exporting context implementation from different project and writing custom rx wrappers...


* be aware that current MyICPC doesn't support multi-part data upload, so video & photo upload may not be fully functional...

---

## Installation

First of all, you need either Xcode or AppCode.

Main Dependency manager is [Carthage](https://github.com/Carthage/Carthage), so you need to install it as well.

[CocoaPods](https://cocoapods.org/) are used for all the stuff unavailable with Carthage like SwiftGen and MMLanScan, but those are included into project, so you don't have to install them </br>

1. Run 'carthage bootstrap --platform iOS' (this may take a while) from inside project dir, all scripts and linking are pre-configured, you don't need to change project at all.
2. Change dev cert for your own (if you want to run on real device).
3. IMPORTANT Whichever IDE you choose, you should open project by opening `QuestApp.xcworkspace`, otherwise there would be build fails because of how Pods work... That's why i've included all Pods and SwiftGen generated stuff, so you don't have to mess with it...
4. Command+B (Command+F9 from AppCode), make sure everything builds fine.
5. :tada:

Now.

4. There 2 active branches you may want to have a look at: **development** and **app/the-other-one**
"app/the-other-one" contains dummy data hardcoded into repositories for plug-n-play (except challenges, those are linked to actual api service)
5. To run dummy app with dummy data checkout and setup *env-development* contained in *Environment*
you would need to setup local keycloak installation (the dummy one that they provide will be enough, check my *env-development.plist* for reference), optionally setup your local MyICPC instance to see challenges
6. To run on real production checkout **development** and setup env-production.plist (if you want to play around with both Dev & Prod setups you can as these configs have different identifiers so you can have both apps installed side by side, the dev one will have a "Dev" prefix in it's name)

---

## Dependencies

* [SwiftGen](https://github.com/SwiftGen/SwiftGen) (MIT)
* [MMLanScan](https://github.com/mavris/MMLanScan) (MIT)
* [SnapKit](https://github.com/SnapKit/SnapKit) (MIT)
* [Swinject](https://github.com/Swinject/Swinject) (MIT)
* [AppAuth iOS](https://github.com/openid/AppAuth-iOS) (Apache License 2.0)
* [Locksmith](https://github.com/matthewpalmer/Locksmith) (MIT)
* [Alamofire](https://github.com/Alamofire/Alamofire) (MIT)
* [RxSwift](https://github.com/ReactiveX/RxSwift) (MIT)
* [Charts](https://github.com/danielgindi/Charts) (MIT)
* [SKPhotoBrowser](https://github.com/suzuki-0000/SKPhotoBrowser) (MIT)
* [Gallery](https://github.com/hyperoslo/Gallery) (MIT)


---

## Some Screens (well, almost all of them...)

## Login & Profile

![LoginProfile](https://bitbucket.org/kompotmalinovy/myicpc-quest-ios/raw/ef2ec870ff2c5b6f56d492a520d227b1d754b333/GalleryAssets/gallery_auth.png)

## Notifications

![Notifs](https://bitbucket.org/kompotmalinovy/myicpc-quest-ios/raw/c363f368af71b5fc739da12dae8c18ad0dd84a00/GalleryAssets/gallery_notifications.png)

## Challenges

![Challenges](https://bitbucket.org/kompotmalinovy/myicpc-quest-ios/raw/c363f368af71b5fc739da12dae8c18ad0dd84a00/GalleryAssets/gallery_challenges.png)

## Input

![Input](https://bitbucket.org/kompotmalinovy/myicpc-quest-ios/raw/c363f368af71b5fc739da12dae8c18ad0dd84a00/GalleryAssets/gallery_input.png)

## Leaderboards

![Leaderboard](https://bitbucket.org/kompotmalinovy/myicpc-quest-ios/raw/c363f368af71b5fc739da12dae8c18ad0dd84a00/GalleryAssets/gallery_leaderboard.png)