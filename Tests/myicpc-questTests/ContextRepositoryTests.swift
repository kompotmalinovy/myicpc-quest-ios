//
// Created by Vlad Gorbunov on 22/05/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import XCTest
import RxSwift
import RxBlocking

@testable import myicpc_quest

class ContextRepositoryTest: XCTestCase {
    weak var contextRepository: ContextRepositoring!
    weak var appSettingsManager: AppSettingsManaging!

    override class func setUp() {
        super.setUp()
        appSettingsManager = MockAppSettings()
        contextRepository = ContextRepository(
                appSettings: appSettingsManager,
                deviceProvider: DeviceMockProvider(),
                locationProvider: LocationMockProvider(),
                networkProvider: ConnectedWifiMockProvider(),
                lanDeviceProvider: LanDeviceMockProvider(),
                bleDeviceProvider: BLEDeviceMockProvider()
        )
    }

    // Expected: nil on false, !nil on true
    func testContextFetchOnSettings() {
        appSettingsManager.contextPermissionGranted = false
        do {
            let context = try contextRepository.observeContext().toBlocking().first()!!
            XCTAssertTrue { context == nil }
        } catch {
            XCTFail(error.localizedDescription)
        }

        appSettingsManager.contextPermissionGranted = true
        do {
            XCTAssertTrue { context != nil }
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
}

class BLEDeviceMockProvider: RxBLEDeviceProviding {
    func observeBLEDevices(timeInterval: RxTimeInterval) -> Observable<[BTDevice]> {
        return Observable.just([BTDevice(name: "Stereo Speaker")])
    }
}

class ConnectedWifiMockProvider: RxConnectedNetworkProviding {
    func observeConnectedNetwork() -> Observable<ConnectedWifi?> {
        return Observable.just(ConnectedWifi(ssid: "BOHEMIA", bssid: "8:60:6e:5f:6a:44"))
    }
}

class DeviceMockProvider: RxDeviceProviding {
    func observeDevice() -> Observable<Device> {
        return Observable.just(Device(operatingSystem: "iOS", version: "11.2", deviceBrand: "Apple", deviceModel: "iPhone", serial: nil))
    }
}

class LanDeviceMockProvider: RxLANDeviceProviding {
    func observeLANDevices(timeInterval: RxTimeInterval) -> Observable<[LANDevice]> {
        return Observable.just([LANDevice(ipAddress: "147.0.0.1", macAddress: "02:00:00:00:00")])
    }
}

class LocationMockProvider: RxLocationProviding {
    func observeLocation() -> Observable<Location?> {
        return Observable.just(nil)
    }
}

class MockAppSettings: AppSettingsManaging {
    var launchedBefore: Bool = false
    var contextPermissionRequested: Bool = false
    var contextPermissionGranted: Bool = false
    var user: User? = nil
}
