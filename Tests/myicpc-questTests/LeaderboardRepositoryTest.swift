//
// Created by Vlad Gorbunov on 19/05/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import XCTest
import RxSwift
import RxBlocking

@testable import myicpc_quest

class LeaderboardRepositoryTest: XCTestCase {

    weak var repository: LeaderboardRepositoring!

    // prepare
    override func setUp() {
        super.setUp()
        let api = MockedApiService()
        repository = LeaderboardRepository(apiService: api)
    }

    // dispose
    override func tearDown() {
        super.tearDown()
    }

    // Expected: original types + team leaderboard
    func testLeaderboardTypes() {
        do {
            let types = try repository.observeLeaderboards()
                    .toBlocking().first()!!

            XCTAssertTrue {
                types.count == 3
                &&
                types.contains { $0 == LeaderboardType.team }
            }
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    // Expected: first row corresponds to one with max points
    func testLeaderboardRows() {
        do {
            let rows = try repository.observeLeaderboard(type: LeaderboardType.team)
                    .toBlocking().first()!!

            let maxPoints = rows.first!!.points

            let isMaxedAndSorted = rows.reduce(into: true) { acc, row in
                acc & row.points <= row.maxPoints && row.maxPoints == maxPoints
            }

            XCTAssertTrue(isMaxedAndSorted)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    private class MockedApiService: ApiServicing {

        private func getLeaderboardTypes() -> Observable<[Leaderboard]> {
            return Observable.just(
                    [Leaderboard(name: "Contestant", urlCode: "cont", roles: ["Contestant"])],
                    [Leaderboard(name: "Staff", urlCode: "staf", roles: ["Staff"])]
            )
        }

        private func getLeaderboard(urlCode: String) -> Observable<[LeaderboardRowResponse]> {
            let rows = stride(from: 0, through: 100, by: 2).map { LeaderboardRowResponse(name: "row \($0)", points: $0) }
            return Observable.just(rows)

        }

        private func getFeed() -> Observable<[Notification]> {
            fatalError("getTimeline(contestCode:) has not been implemented")
        }

        private func getTimeline(contestCode: String) -> Observable<[Notification]> {
            fatalError("getTimeline(contestCode:) has not been implemented")
        }

        private func getChallenges() -> Observable<[Challenge]> {
            fatalError("getChallenges() has not been implemented")
        }

        private func getChallenge(hashtagSuffix: String) -> Observable<Challenge> {
            fatalError("getChallenge(hashtagSuffix:) has not been implemented")
        }

        private func getAcceptedSubmissions(hashtagSuffix: String) -> Observable<[NotificationResponse]> {
            fatalError("getAcceptedSubmissions(hashtagSuffix:) has not been implemented")
        }

        private func getPendingSubmissions(hashtagSuffix: String) -> Observable<[NotificationResponse]> {
            fatalError("getPendingSubmissions(hashtagSuffix:) has not been implemented")
        }

        private func getRejectedSubmissions(hashtagSuffix: String) -> Observable<[NotificationResponse]> {
            fatalError("getRejectedSubmissions(hashtagSuffix:) has not been implemented")
        }

        private func getTeamLeaderboard() -> Observable<[LeaderboardRow]> {
            fatalError("getTeamLeaderboard() has not been implemented")
        }

        private func getUser(contestCode: String) -> Observable<User> {
            fatalError("getUser(contestCode:) has not been implemented")
        }

        private func postSubmission(submission: Submission) -> Observable<Void> {
            fatalError("postSubmission(submission:) has not been implemented")
        }
    }
}