//
//  AppDelegate.swift
//  myicpc-quest
//
//  Created by Vlad Gorbunov on 05/03/2018.
//  Copyright © 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let appCoordinator = AppContainer.container.resolve(AppCoordinator.self, argument: UIWindow(frame: UIScreen.main.bounds))

        appCoordinator?.start()
        return true
    }
}