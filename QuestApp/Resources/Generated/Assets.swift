// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSImage
  typealias AssetColorTypeAlias = NSColor
  typealias Image = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  typealias AssetColorTypeAlias = UIColor
  typealias Image = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

@available(*, deprecated, renamed: "ImageAsset")
typealias AssetType = ImageAsset

struct ImageAsset {
  fileprivate var name: String

  var image: Image {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: NSImage.Name(name))
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else { fatalError("Unable to load image named \(name).") }
    return result
  }
}

struct ColorAsset {
  fileprivate var name: String

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  var color: AssetColorTypeAlias {
    return AssetColorTypeAlias(asset: self)
  }
}

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
enum Asset {
  static let icCancelRounded = ImageAsset(name: "icCancelRounded")
  static let icClose = ImageAsset(name: "icClose")
  static let icDate = ImageAsset(name: "icDate")
  static let icImageFile = ImageAsset(name: "icImageFile")
  static let icPickImage2 = ImageAsset(name: "icPickImage2")
  static let icPickVideo = ImageAsset(name: "icPickVideo")
  static let icVideoFile = ImageAsset(name: "icVideoFile")
  static let icHashtag = ImageAsset(name: "ic_hashtag")
  static let icMedal = ImageAsset(name: "ic_medal")
  static let icMore = ImageAsset(name: "ic_more")
  static let icNointernet = ImageAsset(name: "ic_nointernet")
  static let icNotif = ImageAsset(name: "ic_notif")
  static let icPhoto1 = ImageAsset(name: "ic_photo1")
  static let icPlayback = ImageAsset(name: "ic_playback")
  static let icPlayback2 = ImageAsset(name: "ic_playback2")
  static let icPlus = ImageAsset(name: "ic_plus")
  static let icProfilePlaceholder = ImageAsset(name: "ic_profile_placeholder")
  static let icProfilePlaceholderSmall = ImageAsset(name: "ic_profile_placeholder_small")
  static let icRankAttendee = ImageAsset(name: "ic_rank_attendee")
  static let icRankCoach = ImageAsset(name: "ic_rank_coach")
  static let icRankContestant = ImageAsset(name: "ic_rank_contestant")
  static let icRankGeneral = ImageAsset(name: "ic_rank_general")
  static let icRankReserve = ImageAsset(name: "ic_rank_reserve")
  static let icRankStaff = ImageAsset(name: "ic_rank_staff")
  static let icRankTeam = ImageAsset(name: "ic_rank_team")
  static let icSend1 = ImageAsset(name: "ic_send1")
  static let icSend2 = ImageAsset(name: "ic_send2")
  static let icVideo1 = ImageAsset(name: "ic_video1")
  static let icpcCrowd2 = ImageAsset(name: "icpc-crowd2")
  static let icpcLogoSmall = ImageAsset(name: "icpc-logo-small")
  static let icpcLogo = ImageAsset(name: "icpc-logo")
  static let icpcCrowd = ImageAsset(name: "icpc_crowd")
  static let questLogo = ImageAsset(name: "quest-logo")
  static let tabbarIconFeed = ImageAsset(name: "tabbar-icon-feed")
  static let tabbarIconLeaderboardFlat = ImageAsset(name: "tabbar-icon-leaderboard-flat")
  static let tabbarIconQuests = ImageAsset(name: "tabbar-icon-quests")
  static let tabbarIconTimeline = ImageAsset(name: "tabbar-icon-timeline")

  // swiftlint:disable trailing_comma
  static let allColors: [ColorAsset] = [
  ]
  static let allImages: [ImageAsset] = [
    icCancelRounded,
    icClose,
    icDate,
    icImageFile,
    icPickImage2,
    icPickVideo,
    icVideoFile,
    icHashtag,
    icMedal,
    icMore,
    icNointernet,
    icNotif,
    icPhoto1,
    icPlayback,
    icPlayback2,
    icPlus,
    icProfilePlaceholder,
    icProfilePlaceholderSmall,
    icRankAttendee,
    icRankCoach,
    icRankContestant,
    icRankGeneral,
    icRankReserve,
    icRankStaff,
    icRankTeam,
    icSend1,
    icSend2,
    icVideo1,
    icpcCrowd2,
    icpcLogoSmall,
    icpcLogo,
    icpcCrowd,
    questLogo,
    tabbarIconFeed,
    tabbarIconLeaderboardFlat,
    tabbarIconQuests,
    tabbarIconTimeline,
  ]
  // swiftlint:enable trailing_comma
  @available(*, deprecated, renamed: "allImages")
  static let allValues: [AssetType] = allImages
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

extension Image {
  @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
  @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

extension AssetColorTypeAlias {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

private final class BundleToken {}
