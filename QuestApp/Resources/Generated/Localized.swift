// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
enum L10n {

  enum Btn {
    /// Participate
    static let participate = L10n.tr("Localizable", "btn.participate")
    /// Submissions
    static let submissions = L10n.tr("Localizable", "btn.submissions")
  }

  enum Date {
    /// Due: %@
    static func due(_ p1: String) -> String {
      return L10n.tr("Localizable", "date.due", p1)
    }
  }

  enum General {
    /// Sign In with MyICPC
    static let loginText = L10n.tr("Localizable", "general.login_text")
    /// %d pts
    static func points(_ p1: Int) -> String {
      return L10n.tr("Localizable", "general.points", p1)
    }

    enum Action {
      /// Ok
      static let accept = L10n.tr("Localizable", "general.action.accept")
      /// Cancel
      static let cancel = L10n.tr("Localizable", "general.action.cancel")
      /// Logout
      static let logout = L10n.tr("Localizable", "general.action.logout")
    }

    enum Logout {
      /// Make sure to come back!
      static let message = L10n.tr("Localizable", "general.logout.message")
      /// Would you like to logout?
      static let title = L10n.tr("Localizable", "general.logout.title")
    }
  }

  enum Input {

    enum Error {
      /// Your upload failed for some reason, maybe try one more time?
      static let message = L10n.tr("Localizable", "input.error.message")
      /// Hey, mister user:(
      static let title = L10n.tr("Localizable", "input.error.title")
    }

    enum Message {
      /// What's happening?
      static let hint = L10n.tr("Localizable", "input.message.hint")
    }
  }

  enum Leaderboard {
    /// %d pts
    static func points(_ p1: Int) -> String {
      return L10n.tr("Localizable", "leaderboard.points", p1)
    }
    /// Team Leaderboard
    static let teamLeaderboard = L10n.tr("Localizable", "leaderboard.teamLeaderboard")
  }

  enum Navigation {
    /// Challenge
    static let challenge = L10n.tr("Localizable", "navigation.challenge")
    /// Challenges
    static let challenges = L10n.tr("Localizable", "navigation.challenges")
    /// Leaderboards
    static let leaderboards = L10n.tr("Localizable", "navigation.leaderboards")
    /// Profile & Rules
    static let profile = L10n.tr("Localizable", "navigation.profile")

    enum Submissions {
      /// Accepted
      static let accepted = L10n.tr("Localizable", "navigation.submissions.accepted")
      /// Pending
      static let pending = L10n.tr("Localizable", "navigation.submissions.pending")
      /// Rejected
      static let rejected = L10n.tr("Localizable", "navigation.submissions.rejected")
      /// Timeline
      static let timeline = L10n.tr("Localizable", "navigation.submissions.timeline")
      /// Quest Recents
      static let whatsHappeningNow = L10n.tr("Localizable", "navigation.submissions.whatsHappeningNow")
    }
  }

  enum Notification {
    /// #%@
    static func hashtag(_ p1: String) -> String {
      return L10n.tr("Localizable", "notification.hashtag", p1)
    }
    /// @%@
    static func profile(_ p1: String) -> String {
      return L10n.tr("Localizable", "notification.profile", p1)
    }
    /// %@ • %@
    static func source(_ p1: String, _ p2: String) -> String {
      return L10n.tr("Localizable", "notification.source", p1, p2)
    }

    enum Source {
      /// Quest App
      static let mobileApp = L10n.tr("Localizable", "notification.source.mobileApp")
      /// Social
      static let otherSocial = L10n.tr("Localizable", "notification.source.otherSocial")
      /// System
      static let system = L10n.tr("Localizable", "notification.source.system")
      /// Twitter
      static let twitter = L10n.tr("Localizable", "notification.source.twitter")
    }
  }

  enum Permissions {

    enum Action {
      /// OK
      static let allow = L10n.tr("Localizable", "permissions.action.allow")
      /// Don't Allow
      static let dontallow = L10n.tr("Localizable", "permissions.action.dontallow")
    }

    enum Context {
      /// We are conducting a research at ICPC. Allow app to send information about device, network and device location? You can change this setting later in profile menu.
      static let message = L10n.tr("Localizable", "permissions.context.message")
      /// Will you help us out?
      static let title = L10n.tr("Localizable", "permissions.context.title")
    }
  }

  enum Profile {
    /// Logout
    static let logout = L10n.tr("Localizable", "profile.logout")
    /// %@ %@
    static func name(_ p1: String, _ p2: String) -> String {
      return L10n.tr("Localizable", "profile.name", p1, p2)
    }
    /// Quest Rules
    static let rules = L10n.tr("Localizable", "profile.rules")
    /// @%@
    static func username(_ p1: String) -> String {
      return L10n.tr("Localizable", "profile.username", p1)
    }

    enum Settings {
      /// Allow sending contextual device info for ICPC research
      static let context = L10n.tr("Localizable", "profile.settings.context")
    }
  }

  enum Result {

    enum Empty {
      /// No challenges were found
      static let challenges = L10n.tr("Localizable", "result.empty.challenges")
      /// There is no feed right now
      static let feed = L10n.tr("Localizable", "result.empty.feed")
      /// No results were found
      static let general = L10n.tr("Localizable", "result.empty.general")
      /// Try refreshing in a while.
      static let hint = L10n.tr("Localizable", "result.empty.hint")
      /// Seems like no ranks are available
      static let leaderboard = L10n.tr("Localizable", "result.empty.leaderboard")
      /// No leaderboards yet
      static let leaderboards = L10n.tr("Localizable", "result.empty.leaderboards")
      /// Sorry :(
      static let title = L10n.tr("Localizable", "result.empty.title")
    }

    enum Error {

      enum General {
        /// Something happened, not really sure what...
        static let desc = L10n.tr("Localizable", "result.error.general.desc")
        /// Try reinstalling OS (don't)
        static let hint = L10n.tr("Localizable", "result.error.general.hint")
        /// Hmmmm...
        static let title = L10n.tr("Localizable", "result.error.general.title")
      }

      enum Internet {
        /// Looks like we are all alone :(
        static let desc = L10n.tr("Localizable", "result.error.internet.desc")
        /// Check your connection
        static let hint = L10n.tr("Localizable", "result.error.internet.hint")
        /// Ooops...
        static let title = L10n.tr("Localizable", "result.error.internet.title")
      }
    }
  }

  enum Role {
    /// Attendee
    static let attendee = L10n.tr("Localizable", "role.attendee")
    /// Coach
    static let coach = L10n.tr("Localizable", "role.coach")
    /// Cocoach
    static let cocoach = L10n.tr("Localizable", "role.cocoach")
    /// Contestant
    static let contestant = L10n.tr("Localizable", "role.contestant")
    /// Reserve
    static let reserve = L10n.tr("Localizable", "role.reserve")
    /// Staff
    static let staff = L10n.tr("Localizable", "role.staff")
  }

  enum Tabbar {
    /// Leaderboards
    static let leaderboards = L10n.tr("Localizable", "tabbar.leaderboards")
    /// Profile
    static let profile = L10n.tr("Localizable", "tabbar.profile")
    /// Quests
    static let quests = L10n.tr("Localizable", "tabbar.quests")
    /// Timeline
    static let timeline = L10n.tr("Localizable", "tabbar.timeline")
    /// Recents
    static let whatsHappeningNow = L10n.tr("Localizable", "tabbar.whatsHappeningNow")
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
