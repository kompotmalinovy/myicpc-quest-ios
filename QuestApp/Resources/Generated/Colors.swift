// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable operator_usage_whitespace
extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}
// swiftlint:enable operator_usage_whitespace

// swiftlint:disable identifier_name line_length type_body_length
struct ColorName {
  let rgbaValue: UInt32
  var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4b7bec"></span>
  /// Alpha: 100% <br/> (0x4b7becff)
  static let appTint = ColorName(rgbaValue: 0x4b7becff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4a69bd"></span>
  /// Alpha: 100% <br/> (0x4a69bdff)
  static let btnBlue = ColorName(rgbaValue: 0x4a69bdff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#16a085"></span>
  /// Alpha: 100% <br/> (0x16a085ff)
  static let btnGreen = ColorName(rgbaValue: 0x16a085ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4a69bd"></span>
  /// Alpha: 100% <br/> (0x4a69bdff)
  static let circleBorder = ColorName(rgbaValue: 0x4a69bdff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#bdc3c7"></span>
  /// Alpha: 100% <br/> (0xbdc3c7ff)
  static let disabledControl = ColorName(rgbaValue: 0xbdc3c7ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4a69bd"></span>
  /// Alpha: 100% <br/> (0x4a69bdff)
  static let icpcBlue = ColorName(rgbaValue: 0x4a69bdff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#e55039"></span>
  /// Alpha: 100% <br/> (0xe55039ff)
  static let icpcRed = ColorName(rgbaValue: 0xe55039ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f6b93b"></span>
  /// Alpha: 100% <br/> (0xf6b93bff)
  static let icpcYellow = ColorName(rgbaValue: 0xf6b93bff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#dfe4ea"></span>
  /// Alpha: 100% <br/> (0xdfe4eaff)
  static let lightGrey = ColorName(rgbaValue: 0xdfe4eaff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#1e3799"></span>
  /// Alpha: 100% <br/> (0x1e3799ff)
  static let overlayBlue = ColorName(rgbaValue: 0x1e3799ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#26de81"></span>
  /// Alpha: 100% <br/> (0x26de81ff)
  static let successGreen = ColorName(rgbaValue: 0x26de81ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#cdd1d2"></span>
  /// Alpha: 100% <br/> (0xcdd1d2ff)
  static let systemBkg = ColorName(rgbaValue: 0xcdd1d2ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#4a69bd"></span>
  /// Alpha: 100% <br/> (0x4a69bdff)
  static let tabSelected = ColorName(rgbaValue: 0x4a69bdff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#bdc3c7"></span>
  /// Alpha: 100% <br/> (0xbdc3c7ff)
  static let tabUnselected = ColorName(rgbaValue: 0xbdc3c7ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#3a3a3a"></span>
  /// Alpha: 100% <br/> (0x3a3a3aff)
  static let textTitle = ColorName(rgbaValue: 0x3a3a3aff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#f1f2f6"></span>
  /// Alpha: 100% <br/> (0xf1f2f6ff)
  static let warmWhite = ColorName(rgbaValue: 0xf1f2f6ff)
}
// swiftlint:enable identifier_name line_length type_body_length

extension Color {
  convenience init(named color: ColorName) {
    self.init(rgbaValue: color.rgbaValue)
  }
}
