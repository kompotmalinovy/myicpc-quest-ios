//
//  AppFlowController.swift
//  QuestApp
//
//  Created by Vlad Gorbunov on 11/03/2018.
//  Copyright © 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import AppAuth
import RxSwift

final class AppCoordinator: NSObject {
    
    private let window: UIWindow
    private let userManager: UserManaging
    private let appSettings: AppSettingsManaging
    private let loginFactory: LoginViewControllerFactory
    private let feedCoordinator: RecentsCoordinator
    private let questsCoordinator: ChallengeCoordinator
    private let leaderboardCoordinator: LeaderboardCoordinator
    private let timelineCoordinator: TimelineCoordinator

    private let disposeBag = DisposeBag()
    private var currentAuthFlow: OIDAuthorizationFlowSession?

    init(window: UIWindow,
         userManager: UserManaging,
         appSettings: AppSettingsManaging,
         loginFactory: @escaping LoginViewControllerFactory,
         timelineCoordinator: TimelineCoordinator,
         feedCoordinator: RecentsCoordinator,
         questsCoordinator: ChallengeCoordinator,
         leaderboardCoordinator: LeaderboardCoordinator) {

        self.window = window
        self.userManager = userManager
        self.appSettings = appSettings
        self.loginFactory = loginFactory
        self.timelineCoordinator = timelineCoordinator
        self.feedCoordinator = feedCoordinator
        self.questsCoordinator = questsCoordinator
        self.leaderboardCoordinator = leaderboardCoordinator
    }
    
    func start() {
        window.makeKeyAndVisible()
        if userManager.isAuthorized {
            // show main and perform any request,
            // if tokens are not ok they will be either revoked
            // or thrown error will set us back to login view
            showMain()
        } else {
            window.rootViewController = loginFactory(self)
        }

        userManager.observeAuthChanges()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { state in
                    switch state {
                        case .loggedOut:
                            self.currentAuthFlow?.cancel()
                            self.showLogin()
                        default: break
                    }
                })
                .disposed(by: disposeBag)
    }

    private func showLogin() {
        window.rootViewController = loginFactory(self)
    }

    private func showMain() {
        let tabbarController = UITabBarController()
        tabbarController.tabBar.barTintColor = UIColor.white
        tabbarController.tabBar.backgroundColor = UIColor.white
        tabbarController.tabBar.tintColor = UIColor(named: .appTint)
        tabbarController.tabBar.unselectedItemTintColor = UIColor(named: .tabUnselected)

        feedCoordinator.start()
        timelineCoordinator.start()
        questsCoordinator.start()
        leaderboardCoordinator.start()

        tabbarController.viewControllers = [timelineCoordinator.rootViewController,
                                            feedCoordinator.rootViewController,
                                            questsCoordinator.rootViewController,
                                            leaderboardCoordinator.rootViewController]

        window.rootViewController = tabbarController

        if !appSettings.contextPermissionRequested {
            appSettings.contextPermissionRequested = true

            let dialog = UIAlertController(title: L10n.Permissions.Context.title, message: L10n.Permissions.Context.message, preferredStyle: .alert)
            dialog.addAction(UIAlertAction(title: L10n.Permissions.Action.allow, style: .default, handler: { _ in
                self.appSettings.contextPermissionGranted = true
            }))
            dialog.addAction(UIAlertAction(title: L10n.Permissions.Action.dontallow, style: .cancel, handler: { _ in
                self.appSettings.contextPermissionGranted = false
            }))
            window.rootViewController?.present(dialog, animated: true)
        }
    }
}

extension AppCoordinator: LoginNavigationDelegate {

    func didSelectLogin() {
        let request = userManager.authRequest()

        self.currentAuthFlow = OIDAuthState.authState(byPresenting: request, presenting: window.rootViewController!, callback: { (authState, error) in
            if let state = authState {
                self.userManager.updateAuthState(state: state)
                self.showMain()
            }
        })
    }
}
