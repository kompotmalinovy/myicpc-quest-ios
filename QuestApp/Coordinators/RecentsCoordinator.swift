//
// Created by Vlad Gorbunov on 18/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//
import UIKit

final class RecentsCoordinator: NSObject {
    private let recentsFactory: NotificationsViewControllerFactory
    private let profileFactory: ProfileViewControllerFactory

    let rootViewController = UINavigationController()

    init(recentsFactory: @escaping NotificationsViewControllerFactory, profileFactory: @escaping ProfileViewControllerFactory) {
        self.recentsFactory = recentsFactory
        self.profileFactory = profileFactory
    }

    func start() {
        let controller = recentsFactory(self, .whatsHappeningNow)
        controller.tabBarItem.image = Asset.tabbarIconFeed.image.withRenderingMode(.alwaysTemplate)
        controller.tabBarItem.title = L10n.Tabbar.whatsHappeningNow
        rootViewController.viewControllers = [controller]
    }
}

extension RecentsCoordinator: NotificationListNavigationDelegate {

    func didSelect(submission: Notification) {
        // we don't support notification detail display right now
    }

    func didSelectProfile() {
        let controller = profileFactory()
        controller.modalPresentationStyle = .popover

        let navigationController = UINavigationController(rootViewController: controller)
        rootViewController.present(navigationController, animated: true)
    }

    func didSelectPost() {
        // here submit functionality is not supported since we only display accepted/rejecte/.. submissions
    }
}