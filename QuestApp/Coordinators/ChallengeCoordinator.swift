//
// Created by Vlad Gorbunov on 18/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//
import UIKit
import SafariServices

final class ChallengeCoordinator: NSObject {
    private let questsFactory: QuestsViewControllerFactory
    private let challengeFactory: ChallengeViewControllerFactory
    private let notificationsFactory: NotificationsViewControllerFactory
    private let profileFactory: ProfileViewControllerFactory
    private let inputFactory: InputViewControllerFactory
    let rootViewController = UINavigationController()

    init(questsFactory: @escaping QuestsViewControllerFactory,
         challengeFactory: @escaping ChallengeViewControllerFactory,
         notificationsFactory: @escaping NotificationsViewControllerFactory,
         profileFactory: @escaping ProfileViewControllerFactory,
         inputFactory: @escaping InputViewControllerFactory) {
        self.questsFactory = questsFactory
        self.challengeFactory = challengeFactory
        self.notificationsFactory = notificationsFactory
        self.profileFactory = profileFactory
        self.inputFactory = inputFactory
    }

    func start() {
        let controller = questsFactory(self)
        controller.tabBarItem.image = Asset.tabbarIconQuests.image.withRenderingMode(.alwaysTemplate)
        controller.tabBarItem.title = L10n.Tabbar.quests
        rootViewController.viewControllers = [controller]
    }
}

extension ChallengeCoordinator: ChallengeListNavigationDelegate {

    func didSelect(challenge: Challenge) {
        let controller = challengeFactory(self, false, challenge)
        rootViewController.pushViewController(controller, animated: true)
    }

    func didSelectProfile() {
        let controller = profileFactory()
        controller.modalPresentationStyle = .popover

        let navigationController = UINavigationController(rootViewController: controller)
        rootViewController.present(navigationController, animated: true)
    }
}

extension ChallengeCoordinator: ChallengeNavigationDelegate {

    func didSelectParticipate(in challenge: Challenge) {
        let controller = inputFactory(.challenge(challenge))
        controller.modalPresentationStyle = .popover

        let navigationController = UINavigationController(rootViewController: controller)
        rootViewController.present(navigationController, animated: true)
    }

    func didSelectChallengeNotifications(for challenge: Challenge) {
        let controller = ModalNotificationsViewController(challenge: challenge, factory: notificationsFactory)
        controller.modalPresentationStyle = .popover

        let navigationController = UINavigationController(rootViewController: controller)
        rootViewController.present(navigationController, animated: true)
    }
}