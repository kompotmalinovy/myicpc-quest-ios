//
// Created by Vlad Gorbunov on 17/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit

class TimelineCoordinator: NSObject {
    private let timelineFactory: NotificationsViewControllerFactory
    private let profileFactory: ProfileViewControllerFactory
    private let inputFactory: InputViewControllerFactory

    let rootViewController = UINavigationController()

    init(timelineFactory: @escaping NotificationsViewControllerFactory, profileFactory: @escaping ProfileViewControllerFactory, inputFactory: @escaping InputViewControllerFactory) {
        self.timelineFactory = timelineFactory
        self.profileFactory = profileFactory
        self.inputFactory = inputFactory
    }

    func start() {
        let controller = timelineFactory(self, .timeline)
        controller.tabBarItem.image = Asset.tabbarIconTimeline.image.withRenderingMode(.alwaysTemplate)
        controller.title = L10n.Tabbar.timeline

        rootViewController.viewControllers = [controller]
    }
}

extension TimelineCoordinator: NotificationListNavigationDelegate {

    func didSelect(submission: Notification) {
        // we don't support notification detail display right now
    }

    func didSelectProfile() {
        let controller = profileFactory()
        controller.modalPresentationStyle = .popover

        let navigationController = UINavigationController(rootViewController: controller)
        rootViewController.present(navigationController, animated: true)
    }

    func didSelectPost() {
        let controller = inputFactory(.timeline)
        controller.modalPresentationStyle = .popover

        let navigationController = UINavigationController(rootViewController: controller)
        rootViewController.present(navigationController, animated: true)
    }
}