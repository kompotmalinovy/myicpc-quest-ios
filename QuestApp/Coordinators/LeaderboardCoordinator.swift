//
// Created by Vlad Gorbunov on 07/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit

final class LeaderboardCoordinator: NSObject {
    private let listFactory: LeaderboardListViewControllerFactory
    private let leaderboardFactory: LeaderboardViewControllerFactory
    private let profileFactory: ProfileViewControllerFactory
    let rootViewController = UINavigationController()

    init(listFactory: @escaping LeaderboardListViewControllerFactory,
         leaderboardFactory: @escaping LeaderboardViewControllerFactory,
         profileFactory: @escaping ProfileViewControllerFactory) {
        self.listFactory = listFactory
        self.leaderboardFactory = leaderboardFactory
        self.profileFactory = profileFactory
    }

    func start() {
        let controller = listFactory(self)
        controller.tabBarItem.image = Asset.tabbarIconLeaderboardFlat.image.withRenderingMode(.alwaysTemplate)
        controller.tabBarItem.title = L10n.Tabbar.leaderboards
        rootViewController.viewControllers = [controller]
    }
}

extension LeaderboardCoordinator: LeaderboardListNavigationDelegate {

    func didSelect(type: LeaderboardType) {
        let controller = leaderboardFactory(self, type)
        rootViewController.pushViewController(controller, animated: true)
    }

    func didSelectProfile() {
        let controller = profileFactory()
        controller.modalPresentationStyle = .popover

        let navigationController = UINavigationController(rootViewController: controller)
        rootViewController.present(navigationController, animated: true)
    }
}