//
// Created by Vlad Gorbunov on 29/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import MMLanScan
import RxSwift

protocol RxLanScanning: class {
    func observeLANDevices() -> Observable<MMDevice>
}

class RxLanScanner: NSObject, MMLANScannerDelegate, RxLanScanning {
    private let deviceSubject: PublishSubject<MMDevice> = PublishSubject<MMDevice>()
    private var scanner: MMLANScanner!

    override init() {
        super.init()
        scanner = MMLANScanner(delegate: self)
    }

    func lanScanDidFindNewDevice(_ device: MMDevice!) {
        deviceSubject.on(.next(device))
    }

    func lanScanDidFinishScanning(with status: MMLanScannerStatus) {
        deviceSubject.on(.completed)
    }

    func lanScanDidFailedToScan() {
        deviceSubject.on(.error(LanScanError.failedToScan))
    }

    // MARK: RxLanScanning
    func observeLANDevices() -> Observable<MMDevice> {
        return deviceSubject.asObservable()
                .do(onSubscribe: { self.scanner.start() })
                .do(onDispose: { self.scanner.stop() })
    }
}

enum LanScanError: Error {
    case failedToScan
}