//
//  UIExtensions.swift
//  QuestApp
//
//  Created by Vlad Gorbunov on 11/03/2018.
//  Copyright © 2018 Vlad Gorbunov. All rights reserved.
//
import UIKit

extension UIButton {
    func setBackgroundColor(color: UIColor, for state: UIControlState) {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        UIGraphicsGetCurrentContext()!.setFillColor(color.cgColor)
        UIGraphicsGetCurrentContext()!.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.setBackgroundImage(colorImage, for: state)
    }
}
