//
// Created by Vlad Gorbunov on 18/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

/*
    Helper class for loading video thumbnails/preview images.
*/
class VideoThumbnailHelper {

    static let shared = VideoThumbnailHelper()

    private var imageCache: [String:UIImage] = [:]

    private init() {}

    func loadThumbnail(for url: URL) -> UIImage {
        if let image = imageCache[url.absoluteString] {
            return image
        } else {
            let asset = AVAsset(url: url)
            let generator = AVAssetImageGenerator(asset: asset)
            do {
                let thumbnail = try generator.copyCGImage(at: CMTimeMake(asset.duration.value / 2, asset.duration.timescale), actualTime: nil)
                let thumbnailImage = UIImage(cgImage: thumbnail)
                imageCache[url.absoluteString] = thumbnailImage
                return thumbnailImage
            } catch let exception {
                print(exception)
                return Asset.icProfilePlaceholder.image
            }
        }
    }
}