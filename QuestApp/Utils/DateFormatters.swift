//
// Created by Vlad Gorbunov on 18/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

class DateFormatters {

    static let sharedDayMonthTime: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("HH:mm dd. MMM.")
        return dateFormatter
    }()

    static let sharedDayMonth: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.setLocalizedDateFormatFromTemplate("dd. MMMM")
        return dateFormatter
    }()
}