//
// Created by Vlad Gorbunov on 21/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import Photos
import RxSwift

extension PHAsset {
    func ext_toUIImage(resizeMode: PHImageRequestOptionsResizeMode = .exact, completion: @escaping (_ result: UIImage?) -> Void) {
        let options = PHImageRequestOptions()
        options.resizeMode = resizeMode

        let manager = PHImageManager.default()
        manager.requestImage(
                for: self,
                targetSize: PHImageManagerMaximumSize,
                contentMode: .default,
                options: options,
                resultHandler: { img, _ in
                    completion(img)
                })
    }

    func ext_toImage(resizeMode: PHImageRequestOptionsResizeMode = .exact) -> Observable<UIImage> {
        return Observable.create { observer in
            self.ext_toUIImage { img in
                observer.onNext(img!)
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }

    func ext_toImageObservable() -> Observable<UIImage> {
        return Observable.create { observer in
            let manager = PHImageManager.default()
            let request = manager.requestImage(for: self, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: nil) { image, info in
                if let image = image {
                    observer.on(.next(image))
                    observer.on(.completed)
                } else {
                    observer.on(.error(ImageAssetError.resourceToImageError))
                }
            }
            return Disposables.create { manager.cancelImageRequest(request) }
        }
    }

    func ext_toAVURLAssetObservable() -> Observable<AVURLAsset> {
        return Observable.create { observer in
            let manager = PHImageManager.default()
            let request = manager.requestAVAsset(forVideo: self, options: nil) { asset, _, _ in
                if let avUrlAsset = asset as? AVURLAsset {
                    observer.on(.next(avUrlAsset))
                    observer.on(.completed)
                } else {
                    observer.on(.error(ImageAssetError.resourceToImageError))
                }
            }
            return Disposables.create { manager.cancelImageRequest(request) }
        }
    }
}

enum ImageAssetError: Error {
    case resourceToImageError
}