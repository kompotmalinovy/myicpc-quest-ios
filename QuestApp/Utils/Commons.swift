//
// Created by Vlad Gorbunov on 19/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import RxSwift
import UIKit

extension Bool {
    func not() -> Bool {
        return !self
    }
}

func keyboardHeight() -> Observable<CGFloat> {
    return Observable
            .from([
                NotificationCenter.default.rx.notification(NSNotification.Name.UIKeyboardWillShow)
                        .map { notification -> CGFloat in
                            (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height ?? 0
                        },
                NotificationCenter.default.rx.notification(NSNotification.Name.UIKeyboardWillHide)
                        .map { _ -> CGFloat in
                            0
                        }
            ])
            .merge()
}