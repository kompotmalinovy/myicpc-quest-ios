//
// Created by Vlad Gorbunov on 17/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import CoreGraphics

extension UIRefreshControl {
    func ext_setRefreshing(_ isRefreshing: Bool) {
        isRefreshing ? beginRefreshing() : endRefreshing()
    }
}

extension UIActivityIndicatorView {
    func ext_setAnimating(_ isAnimating: Bool) {
        isAnimating ? startAnimating() : stopAnimating()
    }
}

extension UIColor {
    func image() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        let context = UIGraphicsGetCurrentContext()!

        context.setFillColor(self.cgColor)
        context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))

        let colorImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return colorImage!
    }

    public func darkened(by amount: CGFloat = 0.25) -> UIColor {
        return with(brightnessAmount: 1 - max(0, amount))
    }

    fileprivate func with(brightnessAmount: CGFloat) -> UIColor {
        var h: CGFloat = 0
        var s: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0

        guard getHue(&h, saturation: &s, brightness: &b, alpha: &a) else { return self }

        return UIColor(
                hue: h,
                saturation: s,
                brightness: b * brightnessAmount,
                alpha: a
        )
    }
}

extension UIImageView {
    func ext_loadVideoThumbnail(url: URL) {
        DispatchQueue.global().async {
            let image = VideoThumbnailHelper.shared.loadThumbnail(for: url)
            DispatchQueue.main.async {
                self.image = image
            }
        }
    }
}

extension UITableViewController {
    func scrollToTop() {
        if tableView.scrollsToTop == true {
            tableView.setContentOffset(CGPoint(x: 0.0, y: -tableView.contentInset.top), animated: true)
        }
    }
}