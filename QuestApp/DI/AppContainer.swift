//
// Created by Vlad Gorbunov on 18/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import Swinject
import UIKit

typealias LoginViewControllerFactory = (_ coordinator: LoginNavigationDelegate) -> LoginViewController
typealias QuestsViewControllerFactory = (_ coordinator: ChallengeCoordinator) -> ChallengeListViewController
typealias ChallengeViewControllerFactory = (_ coordinator: ChallengeCoordinator, _ loadFromInternet: Bool, _ challenge: Challenge) -> ChallengeViewController
typealias LeaderboardListViewControllerFactory = (_ coordinator: LeaderboardCoordinator) -> LeaderboardListViewController
typealias LeaderboardViewControllerFactory = (_ coordinator: LeaderboardCoordinator, _ type: LeaderboardType) -> LeaderboardViewController
typealias ProfileViewControllerFactory = () -> ProfileViewController
typealias NotificationsViewControllerFactory = (_ coordinator: NotificationListNavigationDelegate, _ mode: NotificationViewMode) -> NotificationsViewController
typealias InputViewControllerFactory = (_ type: InputType) -> InputViewController

/**
 Container defining dependency graph of whole application.
*/
class AppContainer {

    static let container = Container() { container in

        // ------------- Managers -----------------
        container.register(KeychainManaging.self) { _ in KeychainManager() }.inObjectScope(.container)

        container.register(AppSettingsManaging.self) { _ in UserDefaults.standard }.inObjectScope(.container)

        container.register(UserManaging.self) { r in
            UserManager(keychainManager: r.resolve(KeychainManaging.self)!, settingsManager: r.resolve(AppSettingsManaging.self)!)
        }.inObjectScope(.container)


        // ------------- Api Service -----------------
        container.register(ApiInteracting.self) { r in
            ApiInteractor(userManager: r.resolve(UserManaging.self)!, apiDescription: ApiDescription())
        }.inObjectScope(.container)



        // ------------- Repositories -----------------
        container.register(ContextRepositoring.self) { r in
            ContextRepository(
                    appSettings: r.resolve(AppSettingsManaging.self)!,
                    deviceProvider: RxDeviceProvider(),
                    locationProvider: RxLocationProvider(),
                    networkProvider: RxConnectedNetworkProvider(),
                    lanDeviceProvider: RxLANDeviceProvider(),
                    bleDeviceProvider: RxBLEDeviceProvider()
            )
        }.inObjectScope(.container)

        container.register(LeaderboardRepositoring.self) { r in
            LeaderboardRepository(apiInteractor: r.resolve(ApiInteracting.self)!)
        }.inObjectScope(.container)

        container.register(ChallengeRepositoring.self) { r in
            ChallengeRepository(apiInteractor: r.resolve(ApiInteracting.self)!)
        }.inObjectScope(.container)

        container.register(NotificationRepositoring.self) { r in
            NotificationRepository(apiInteractor: r.resolve(ApiInteracting.self)!, contextRepository: r.resolve(ContextRepositoring.self)!)
        }.inObjectScope(.container)

        container.register(UserRepositoring.self) { r in
            UserRepository(apiInteractor: r.resolve(ApiInteracting.self)!)
        }.inObjectScope(.container)



        // ------------- Coordinators -----------------
        container.register(AppCoordinator.self) { resolver, window in
            AppCoordinator(
                    window: window,
                    userManager: resolver.resolve(UserManaging.self)!,
                    appSettings: resolver.resolve(AppSettingsManaging.self)!,
                    loginFactory: resolver.resolve(LoginViewControllerFactory.self)!,
                    timelineCoordinator: resolver.resolve(TimelineCoordinator.self)!,
                    feedCoordinator: resolver.resolve(RecentsCoordinator.self)!,
                    questsCoordinator: resolver.resolve(ChallengeCoordinator.self)!,
                    leaderboardCoordinator: resolver.resolve(LeaderboardCoordinator.self)!
            )
        }.inObjectScope(.container)

        container.register(RecentsCoordinator.self) { r in
            RecentsCoordinator(
                    recentsFactory: r.resolve(NotificationsViewControllerFactory.self)!,
                    profileFactory: r.resolve(ProfileViewControllerFactory.self)!
            )
        }

        container.register(ChallengeCoordinator.self) { r in
            ChallengeCoordinator(
                    questsFactory: r.resolve(QuestsViewControllerFactory.self)!,
                    challengeFactory: r.resolve(ChallengeViewControllerFactory.self)!,
                    notificationsFactory: r.resolve(NotificationsViewControllerFactory.self)!,
                    profileFactory: r.resolve(ProfileViewControllerFactory.self)!,
                    inputFactory: r.resolve(InputViewControllerFactory.self)!
            )
        }

        container.register(LeaderboardCoordinator.self) { r in
            LeaderboardCoordinator(
                    listFactory: r.resolve(LeaderboardListViewControllerFactory.self)!,
                    leaderboardFactory: r.resolve(LeaderboardViewControllerFactory.self)!,
                    profileFactory: r.resolve(ProfileViewControllerFactory.self)!
            )
        }

        container.register(TimelineCoordinator.self) { r in
            TimelineCoordinator(
                    timelineFactory: r.resolve(NotificationsViewControllerFactory.self)!,
                    profileFactory: r.resolve(ProfileViewControllerFactory.self)!,
                    inputFactory: r.resolve(InputViewControllerFactory.self)!
            )
        }



        // ------------- ViewModels -----------------
        container.register(ChallengeListViewModel.self) { r in
            ChallengeListViewModel(repository: r.resolve(ChallengeRepositoring.self)!)
        }

        container.register(ChallengeViewModel.self) { (r: Resolver, loadFromInternet: Bool, challenge: Challenge) in
            ChallengeViewModel(loadFromServer: loadFromInternet, challenge: challenge, repository: r.resolve(ChallengeRepositoring.self)!)
        }
        container.register(NotificationsViewModel.self) { (r: Resolver, mode: NotificationViewMode) in
            NotificationsViewModel(mode: mode, repository: r.resolve(NotificationRepositoring.self)!)
        }

        container.register(LeaderboardListViewModel.self) { r in
            LeaderboardListViewModel(repository: r.resolve(LeaderboardRepositoring.self)!)
        }

        container.register(LeaderboardViewModel.self) { (r: Resolver, type: LeaderboardType) in
            LeaderboardViewModel(leaderboardType: type, repository: r.resolve(LeaderboardRepositoring.self)!)
        }

        container.register(ProfileViewModel.self) { r in
            ProfileViewModel(userManager: r.resolve(UserManaging.self)!, appSettings: r.resolve(AppSettingsManaging.self)!, repository: r.resolve(UserRepositoring.self)!)
        }

        container.register(InputViewModel.self) { (r: Resolver, type: InputType) in
            InputViewModel(type: type, repository: r.resolve(NotificationRepositoring.self)!)
        }



        // ------------- ViewControllers -----------------
        container.register(LoginViewControllerFactory.self) { r in
            return { coordinator in
                let loginVC = LoginViewController()
                loginVC.navigator = coordinator
                return loginVC
            }
        }

        container.register(NotificationsViewControllerFactory.self) { r in
            return { coordinator, mode in
                let controller = NotificationsViewController(
                        viewModel: r.resolve(NotificationsViewModel.self, argument: mode)!,
                        profileViewModel: r.resolve(ProfileViewModel.self)!
                )
                controller.navigationDelegate = coordinator
                return controller
            }
        }

        container.register(QuestsViewControllerFactory.self) { r in
            return { coordinator in
                let questsVC = ChallengeListViewController(
                        viewModel: r.resolve(ChallengeListViewModel.self)!,
                        profileViewModel: r.resolve(ProfileViewModel.self)!
                )
                questsVC.navigationDelegate = coordinator
                return questsVC
            }
        }

        container.register(ChallengeViewControllerFactory.self) { r in
            return { coordinator, loadFromServer, challenge in
                let controller = ChallengeViewController(viewModel: r.resolve(ChallengeViewModel.self, arguments: loadFromServer, challenge)!)
                controller.navigationDelegate = coordinator
                return controller
            }
        }

        container.register(LeaderboardListViewControllerFactory.self) { r in
            return { coordinator in
                let vc = LeaderboardListViewController(
                        viewModel: r.resolve(LeaderboardListViewModel.self)!,
                        profileViewModel: r.resolve(ProfileViewModel.self)!
                )
                vc.navigationDelegate = coordinator
                return vc
            }
        }

        container.register(LeaderboardViewControllerFactory.self) { r in
            return { coordinator, type in
                LeaderboardViewController(viewModel: r.resolve(LeaderboardViewModel.self, argument: type)!)
            }
        }

        container.register(ProfileViewControllerFactory.self) { r in
            return {
                ProfileViewController(viewModel: r.resolve(ProfileViewModel.self)!)
            }
        }

        container.register(InputViewControllerFactory.self) { r in
            return { type in
                InputViewController(viewModel: r.resolve(InputViewModel.self, argument: type)!, profileViewModel: r.resolve(ProfileViewModel.self)!)
            }
        }
    }
}
