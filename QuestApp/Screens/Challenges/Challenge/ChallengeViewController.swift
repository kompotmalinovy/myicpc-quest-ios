//
// Created by Vlad Gorbunov on 10/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import AlamofireImage
import Alamofire
import Foundation


protocol ChallengeNavigationDelegate: class {
    func didSelectParticipate(in challenge: Challenge)
    func didSelectChallengeNotifications(for challenge: Challenge)
}

class ChallengeViewController: UIViewController {

    private let disposeBag: DisposeBag = DisposeBag()
    private let viewModel: ChallengeViewModel

    private weak var scrollView: UIScrollView!
    private weak var contentView: UIView!
    private weak var challengeImage: UIImageView!
    private weak var titleLabel: UILabel!
    private weak var descriptionLabel: UILabel!
    private weak var hashtagLabel: UILabel!
    private weak var photoRequiredWrapper: UIView!
    private weak var videoRequiredWrapper: UIView!
    private weak var dueLabel: UILabel!
    private weak var pointsLabel: UILabel!

    weak var navigationDelegate: ChallengeNavigationDelegate?

    init(viewModel: ChallengeViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadChallenge()

        viewModel.observeChallenge()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { state in
                    switch state {
                    case .loaded(let challenge):
                        self.setupView(for: challenge)
                    default:
                        break
                    }
                })
                .disposed(by: disposeBag)
    }


    // MARK: private methods

    private func setupView(for challenge: Challenge) {
        navigationItem.title = L10n.Navigation.challenge
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: Asset.icSend2.image, style: .plain, target: self, action: #selector(onParticipate))

        if let url = challenge.imageUrl {
            self.challengeImage.af_setImage(withURL: url, completion: { response in
                if let size = response.value?.size {
                    self.challengeImage.snp.remakeConstraints { make in
                        make.top.leading.trailing.equalToSuperview()
                        make.height.equalTo(self.contentView.snp.width).multipliedBy(size.height / size.width)
                    }
                }
            })
        }

        titleLabel.text = challenge.name
        descriptionLabel.text = challenge.description
        hashtagLabel.text = challenge.hashtagPrefix + challenge.hashtagSuffix
        photoRequiredWrapper.isHidden = !challenge.requiresPhoto
        videoRequiredWrapper.isHidden = !challenge.requiresVideo
        dueLabel.text = L10n.Date.due(DateFormatters.sharedDayMonth.string(from: challenge.endDate))
        pointsLabel.text = L10n.Leaderboard.points(challenge.defaultPoints)
        contentView.isHidden = false

        let dismissButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        dismissButton.tintColor = UIColor(named: .appTint)
        self.navigationItem.backBarButtonItem = dismissButton
    }

    @objc private func onShowNotifications() {
        if let challenge = viewModel.data, let navDelegate = navigationDelegate {
            navDelegate.didSelectChallengeNotifications(for: challenge)
        }
    }

    @objc private func onParticipate() {
        if let challenge = viewModel.data, let navDelegate = navigationDelegate {
            navDelegate.didSelectParticipate(in: challenge)
        }
    }


    // MARK: view definition
    override func loadView() {
        super.loadView()
        view.backgroundColor = .white

        let scrollView = UIScrollView()
        scrollView.delaysContentTouches = false
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.scrollView = scrollView

        let contentView = UIView()
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(12)
            make.top.bottom.equalToSuperview().inset(20)
            make.width.equalToSuperview().inset(12)
        }
        self.contentView = contentView

        let image = UIImageView()
        image.layer.cornerRadius = 6.0
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFit
        image.backgroundColor = UIColor(named: .disabledControl)
        contentView.addSubview(image)
        image.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
        }
        self.challengeImage = image

        let title = UILabel()
        title.font = UIFont.systemFont(ofSize: 26.0, weight: .bold)
        title.textColor = UIColor(named: .textTitle)
        title.numberOfLines = 0
        contentView.addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalTo(image.snp.bottom).offset(15)
            make.leading.trailing.equalToSuperview()
        }
        self.titleLabel = title

        let description = UILabel()
        description.font = UIFont.systemFont(ofSize: 16.0)
        description.textColor = UIColor(named: .textTitle).withAlphaComponent(0.8)
        description.numberOfLines = 0
        contentView.addSubview(description)
        description.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(5)
            make.leading.trailing.equalToSuperview()
        }
        self.descriptionLabel = description

        let submissionsBtn = UIButton(type: .system)
        submissionsBtn.layer.cornerRadius = 6
        submissionsBtn.clipsToBounds = true
        submissionsBtn.backgroundColor = UIColor(named: .btnGreen)
        submissionsBtn.setTitle(L10n.Btn.submissions, for: .normal)
        submissionsBtn.setTitleColor(.white, for: .normal)
        submissionsBtn.setBackgroundImage(UIColor(named: .btnGreen).image(), for: .normal)
        submissionsBtn.setBackgroundImage(UIColor(named: .btnGreen).darkened().image(), for: .highlighted)
        submissionsBtn.addTarget(self, action: #selector(onShowNotifications), for: .touchUpInside)
        contentView.addSubview(submissionsBtn)
        submissionsBtn.snp.makeConstraints { make in
            make.top.equalTo(description.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(40)
        }

        let participateBtn = UIButton(type: .system)
        participateBtn.layer.cornerRadius = 6
        participateBtn.clipsToBounds = true
        participateBtn.backgroundColor = UIColor(named: .btnBlue)
        participateBtn.setTitle(L10n.Btn.participate, for: .normal)
        participateBtn.setTitleColor(.white, for: .normal)
        participateBtn.setBackgroundImage(UIColor(named: .btnBlue).image(), for: .normal)
        participateBtn.setBackgroundImage(UIColor(named: .btnBlue).darkened().image(), for: .highlighted)
        participateBtn.addTarget(self, action: #selector(onParticipate), for: .touchUpInside)
        contentView.addSubview(participateBtn)
        participateBtn.snp.makeConstraints { make in
            make.top.equalTo(submissionsBtn.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(40)
        }

        let infoStack = UIStackView()
        infoStack.axis = .vertical
        infoStack.spacing = 7
        infoStack.distribution = .fill
        contentView.addSubview(infoStack)
        infoStack.snp.makeConstraints { make in
            make.top.equalTo(participateBtn.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().inset(20)
        }

        let hashtagStack = UIStackView()
        hashtagStack.axis = .horizontal
        hashtagStack.spacing = 7
        infoStack.addArrangedSubview(hashtagStack)

        let hashtagIcon = UIImageView()
        hashtagIcon.image = UIImage(asset: Asset.icHashtag)
        hashtagStack.addArrangedSubview(hashtagIcon)
        hashtagIcon.snp.makeConstraints { make in
            make.width.height.equalTo(32)
        }

        let hashtag = UILabel()
        hashtag.font = UIFont.systemFont(ofSize: 24.0, weight: .medium)
        hashtag.textColor = UIColor(named: .textTitle).withAlphaComponent(0.9)
        hashtagStack.addArrangedSubview(hashtag)
        self.hashtagLabel = hashtag

        let photoStack = UIStackView()
        photoStack.axis = .horizontal
        photoStack.spacing = 7
        infoStack.addArrangedSubview(photoStack)

        let photoIcon = UIImageView()
        photoIcon.image = UIImage(asset: Asset.icPhoto1)
        photoStack.addArrangedSubview(photoIcon)
        photoIcon.snp.makeConstraints { make in
            make.width.height.equalTo(32)
        }

        let photoLabel = UILabel()
        photoLabel.text = "Requires photo"
        photoLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        photoLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.6)
        photoStack.addArrangedSubview(photoLabel)

        self.photoRequiredWrapper = photoStack

        let videoStack = UIStackView()
        videoStack.axis = .horizontal
        videoStack.spacing = 7
        infoStack.addArrangedSubview(videoStack)

        let videoIcon = UIImageView()
        videoIcon.image = UIImage(asset: Asset.icVideo1)
        videoStack.addArrangedSubview(videoIcon)
        videoIcon.snp.makeConstraints { make in
            make.width.height.equalTo(32)
        }

        let videoLabel = UILabel()
        videoLabel.text = "Requires video"
        videoLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        videoLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.6)
        videoStack.addArrangedSubview(videoLabel)
        self.videoRequiredWrapper = videoStack


        let dateStack = UIStackView()
        dateStack.axis = .horizontal
        dateStack.spacing = 7
        infoStack.addArrangedSubview(dateStack)

        let dateIcon = UIImageView()
        dateIcon.image = UIImage(asset: Asset.icDate)
        dateStack.addArrangedSubview(dateIcon)
        dateIcon.snp.makeConstraints { make in
            make.width.height.equalTo(32)
        }

        let dateLabel = UILabel()
        dateLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        dateLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.6)
        dateStack.addArrangedSubview(dateLabel)
        self.dueLabel = dateLabel


        let pointsStack = UIStackView()
        pointsStack.axis = .horizontal
        pointsStack.spacing = 7
        infoStack.addArrangedSubview(pointsStack)

        let pointsIcon = UIImageView()
        pointsIcon.image = UIImage(asset: Asset.icMedal)
        pointsStack.addArrangedSubview(pointsIcon)
        pointsIcon.snp.makeConstraints { make in
            make.width.height.equalTo(32)
        }

        let pointsLabel = UILabel()
        pointsLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        pointsLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.6)
        pointsStack.addArrangedSubview(pointsLabel)
        self.pointsLabel = pointsLabel

        contentView.isHidden = true
    }
}