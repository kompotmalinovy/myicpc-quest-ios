//
// Created by Vlad Gorbunov on 10/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//
import RxSwift

class ChallengeViewModel: ViewModel {

    private let challengeVariable = Variable(State<Challenge>.idle)
    private let repository: ChallengeRepositoring
    private let challenge: Challenge
    private let loadFromServer: Bool

    var data: Challenge?

    init(loadFromServer: Bool, challenge: Challenge, repository: ChallengeRepositoring) {
        self.loadFromServer = loadFromServer
        self.challenge = challenge
        self.repository = repository
    }

    func loadChallenge() {
        if loadFromServer {
            loadChallengeFromRepo()
        } else {
            challengeVariable.value = .loaded(challenge)
        }
    }

    func observeChallenge() -> Observable<State<Challenge>> {
        return challengeVariable.asObservable().do(onNext: { state in
            if case .loaded(let challenge) = state {
                self.data = challenge
            }
        })
    }

    private func loadChallengeFromRepo() {
        challengeVariable.value = .loading
        repository.observeChallenge(hashtagSuffix: challenge.hashtagSuffix)
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .delay(2, scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe { event in
                    switch event {
                    case .next(let challenge):
                        self.challengeVariable.value = .loaded(challenge)
                    case .error(let error):
                        self.challengeVariable.value = .error(error)
                    case .completed:
                        break
                    }
                }
                .disposed(by: disposeBag)
    }
}