//
// Created by Vlad Gorbunov on 21/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import RxSwift

class ChallengeListViewModel: ViewModel {

    private let challengeVariable = Variable(State<[Challenge]>.idle)
    private let repository: ChallengeRepositoring
    var data: [Challenge] = []

    init(repository: ChallengeRepositoring) {
        self.repository = repository
    }

    func loadChallenges() {
        challengeVariable.value = .loading
        updateChallengeState()
    }

    func reloadChallenges() {
        challengeVariable.value = .reloading
        updateChallengeState()
    }

    private func updateChallengeState() {
        repository.observeChallenges()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe { event in
                    switch event {
                    case .next(let challenges):
                        self.challengeVariable.value = challenges.isEmpty ? .empty : .loaded(challenges)
                    case .error(let error):
                        self.challengeVariable.value = .error(error)
                    case .completed:
                        break
                    }
                }
                .disposed(by: disposeBag)
    }

    func observeChallenges() -> Observable<State<[Challenge]>> {
        return challengeVariable.asObservable().do(onNext: {
            if case .loaded(let data) = $0 {
                self.data = data
            } else if case .empty = $0 {
                self.data = []
            }
        })
    }
}