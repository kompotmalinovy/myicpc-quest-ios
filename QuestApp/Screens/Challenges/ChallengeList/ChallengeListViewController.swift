//
// Created by Vlad Gorbunov on 17/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

protocol ChallengeListNavigationDelegate: class {
    func didSelect(challenge: Challenge)
    func didSelectProfile()
}

class ChallengeListViewController: BaseListViewController {

    private let viewModel: ChallengeListViewModel
    private let profileViewModel: ProfileViewModel

    weak var navigationDelegate: ChallengeListNavigationDelegate?


    init(viewModel: ChallengeListViewModel, profileViewModel: ProfileViewModel) {
        self.viewModel = viewModel
        self.profileViewModel = profileViewModel
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadChallenges()

        viewModel.observeChallenges()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { state in
                    self.activityIndicator.ext_setAnimating(state == .loading)
                    self.refreshControl?.ext_setRefreshing(state == .reloading)

                    switch state {
                        case .loaded: self.tableView.backgroundView = nil
                        case .empty: self.handleEmpty(for: L10n.Result.Empty.challenges)
                        case .error(let error): self.handle(error: error)
                        default: break
                    }
                    self.tableView.reloadData()
                })
                .disposed(by: disposeBag)

        profileViewModel.observeUser()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { state in
                    if case .loaded(let user) = state, let pictureUrl = user.profilePictureUrl {
                        let profileButton = self.profileAccessoryView(for: pictureUrl, action: #selector(self.didSelectProfile))
                        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: profileButton)
                    }
                })
                .disposed(by: disposeBag)
    }


    // MARK: BaseListViewController overrides

    override func handleUnauthorized() {
        profileViewModel.logout()
    }


    // MARK: private methods

    @objc private func onRefreshControl() {
        viewModel.reloadChallenges()
    }

    @objc private func didSelectProfile() {
        navigationDelegate?.didSelectProfile()
    }

    private func reuseIdentifier(for challenge: Challenge) -> String {
        return challenge.imageUrl != nil ? ChallengeCell.Identifiers.withImage : ChallengeCell.Identifiers.withoutImage
    }


    // MARK: UITableViewController delegate methods

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier(for: viewModel.data[indexPath.row]), for: indexPath) as! ChallengeCell
        cell.challenge = viewModel.data[indexPath.row]
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationDelegate?.didSelect(challenge: viewModel.data[indexPath.row])
    }


    // MARK: view definition
    override func loadView() {
        super.loadView()
        tableView.register(ChallengeCell.self, forCellReuseIdentifier: ChallengeCell.Identifiers.withoutImage)
        tableView.register(ChallengeCell.self, forCellReuseIdentifier: ChallengeCell.Identifiers.withImage)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 280

        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor(named: .appTint)
        refresh.backgroundColor = UIColor(named: .lightGrey)
        refresh.addTarget(self, action: #selector(onRefreshControl), for: .valueChanged)
        refreshControl = refresh

        navigationItem.title = L10n.Navigation.challenges

        let profileButton = profileAccessoryView(action: #selector(didSelectProfile))
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: profileButton)
    }
}