//
// Created by Vlad Gorbunov on 08/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import AlamofireImage

class BaseListViewController: UITableViewController {

    internal let disposeBag = DisposeBag()
    internal weak var activityIndicator: UIActivityIndicatorView!

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        super.loadView()
        tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        tableView.tableFooterView = UIView(frame: .zero)
        tableView.dataSource = self
        tableView.delegate = self

        tableView.scrollsToTop = true

        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.color = UIColor(named: .appTint)
        view.addSubview(activityIndicator)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        self.activityIndicator = activityIndicator
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let navBackButton = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = navBackButton
    }

    func profileAccessoryView(for imageUrl: URL, action: Selector) -> UIView {
        let profileAccessoryView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 35.0, height: 35.0))
        profileAccessoryView.contentMode = .scaleAspectFill

        let filter = ScaledToSizeWithRoundedCornersFilter(size: CGSize(width: 35, height: 35), radius: 35.0 / 2)
        profileAccessoryView.af_setImage(withURL: imageUrl, placeholderImage: Asset.icProfilePlaceholderSmall.image, filter: filter)

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: action)
        profileAccessoryView.addGestureRecognizer(gestureRecognizer)

        return profileAccessoryView
    }

    func profileAccessoryView(for image: UIImage = Asset.icProfilePlaceholderSmall.image, action: Selector) -> UIView {

        let profileAccessoryView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 35.0, height: 35.0))
        profileAccessoryView.image = image
        profileAccessoryView.contentMode = .scaleAspectFill

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: action)
        profileAccessoryView.addGestureRecognizer(gestureRecognizer)

        return profileAccessoryView
    }

    func handle(error: Error) {
        switch error {
            case AuthError.unauthorized:
                handleUnauthorized()
            default:
                showError(for: error)
        }
    }

    func handleEmpty(for cause: String = L10n.Result.Empty.general) {
        tableView.backgroundView = getEmptyResultView(with: cause)
    }

    internal func handleUnauthorized() {
        preconditionFailure("This method must be overridden")
    }

    private func showError(for cause: Error) {
        tableView.backgroundView = getErrorResultView(with: cause)
    }

    private func getEmptyResultView(with description: String) -> UIView {
        let emptyView = UIView()
        emptyView.backgroundColor = UIColor(named: .warmWhite)

        let contentStack = UIStackView()
        contentStack.axis = .vertical
        contentStack.spacing = 10
        emptyView.addSubview(contentStack)
        contentStack.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.centerX.centerY.equalToSuperview()
        }

        let titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        titleLabel.text = L10n.Result.Empty.title
        titleLabel.textColor = UIColor(named: .textTitle)
        titleLabel.textAlignment = .center
        contentStack.addArrangedSubview(titleLabel)

        let descriptionLabel = UILabel()
        descriptionLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        descriptionLabel.text = description
        descriptionLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.7)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .center
        contentStack.addArrangedSubview(descriptionLabel)

        let hint = UILabel()
        hint.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        hint.text = L10n.Result.Empty.hint
        hint.textColor = UIColor(named: .textTitle).withAlphaComponent(0.7)
        hint.numberOfLines = 0
        hint.textAlignment = .center
        contentStack.addArrangedSubview(hint)
        return emptyView
    }

    private func getErrorResultView(with error: Error) -> UIView {
        let assets: (ImageAsset?, String, String, String) = {
            if let error = error as? NSError, error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet {
                return (Asset.icNointernet, L10n.Result.Error.Internet.title, L10n.Result.Error.Internet.desc, L10n.Result.Error.Internet.hint)
            } else {
                return (nil, L10n.Result.Error.General.title, L10n.Result.Error.General.desc, L10n.Result.Error.General.hint)

            }
        }()

        let errorView = UIView()
        errorView.backgroundColor = UIColor(named: .warmWhite)

        let contentStack = UIStackView()
        contentStack.axis = .vertical
        contentStack.spacing = 10
        contentStack.distribution = .fillProportionally
        errorView.addSubview(contentStack)
        contentStack.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.centerX.centerY.equalToSuperview()
        }

        if let iconAsset = assets.0 {
            let image = UIImageView()
            image.image = UIImage(asset: iconAsset)
            image.contentMode = .scaleAspectFit
            contentStack.addArrangedSubview(image)
            image.snp.makeConstraints { make in
                make.width.height.equalTo(150).priority(.high)
            }
        }

        let titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        titleLabel.text = assets.1
        titleLabel.textColor = UIColor(named: .textTitle)
        titleLabel.textAlignment = .center
        contentStack.addArrangedSubview(titleLabel)

        let descriptionLabel = UILabel()
        descriptionLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        descriptionLabel.text = assets.2
        descriptionLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.7)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .center
        contentStack.addArrangedSubview(descriptionLabel)

        let hint = UILabel()
        hint.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        hint.text = assets.3
        hint.textColor = UIColor(named: .textTitle).withAlphaComponent(0.7)
        hint.numberOfLines = 0
        hint.textAlignment = .center
        contentStack.addArrangedSubview(hint)
        return errorView
    }
}