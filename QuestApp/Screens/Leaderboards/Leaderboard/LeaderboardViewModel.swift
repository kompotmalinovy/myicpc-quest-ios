//
// Created by Vlad Gorbunov on 06/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import RxSwift

class LeaderboardViewModel: ViewModel {

    private let leaderboardVariable = Variable(State<[LeaderboardRow]>.idle)
    private let repository: LeaderboardRepositoring

    let leaderboardType: LeaderboardType
    var data: [LeaderboardRow] = []


    init(leaderboardType: LeaderboardType, repository: LeaderboardRepositoring) {
        self.leaderboardType = leaderboardType
        self.repository = repository
    }

    func loadLeaderboard() {
        leaderboardVariable.value = .loading
        repository.observeLeaderboard(type: leaderboardType)
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe { event in
                    switch event {
                    case .next(let leaderboard):
                        self.leaderboardVariable.value = leaderboard.isEmpty ? .empty : .loaded(leaderboard)
                    case .error(let error):
                        self.leaderboardVariable.value = .error(error)
                    case .completed:
                        break
                    }
                }
                .disposed(by: disposeBag)
    }

    func observeLeaderboard() -> Observable<State<[LeaderboardRow]>> {
        return leaderboardVariable.asObservable().do(onNext: { state in
            if case .loaded(let rows) = state {
                self.data = rows
            } else if case .empty = state {
                self.data = []
            }
        })
    }
}