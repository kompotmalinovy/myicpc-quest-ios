//
// Created by Vlad Gorbunov on 07/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import RxSwift

class LeaderboardViewController: BaseListViewController {

    private let viewModel: LeaderboardViewModel


    init(viewModel: LeaderboardViewModel) {
        self.viewModel = viewModel
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        super.loadView()
        tableView.register(LeaderboardRowCell.self, forCellReuseIdentifier: LeaderboardRowCell.Identifiers.withGraph)
        navigationItem.title = {
            switch viewModel.leaderboardType {
                case .generic(let type): return type.name
                case .team: return L10n.Leaderboard.teamLeaderboard
            }
        }()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.observeLeaderboard()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { state in
                    self.activityIndicator.ext_setAnimating(state == State.loading)
                    switch state {
                        case .loaded: self.tableView.backgroundView = nil
                        case .empty: self.handleEmpty(for: L10n.Result.Empty.leaderboard)
                        case .error(let error): self.handle(error: error)
                        default: break
                    }
                    self.tableView.reloadData()
                })
                .disposed(by: disposeBag)

        viewModel.loadLeaderboard()
    }

    override func handleUnauthorized() {}

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LeaderboardRowCell.Identifiers.withGraph, for: indexPath) as! LeaderboardRowCell
        cell.leaderboardRow = viewModel.data[indexPath.row]
        return cell
    }
}