//
// Created by Vlad Gorbunov on 06/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import RxSwift

class LeaderboardListViewModel: ViewModel {
    private let leaderboardsVariable = Variable(State<[LeaderboardType]>.idle)
    private let repository: LeaderboardRepositoring

    var data: [LeaderboardType] = []


    init(repository: LeaderboardRepositoring) {
        self.repository = repository
    }

    func reloadLeaderboardTypes() {
        leaderboardsVariable.value = .reloading
        loadLeaderboards()
    }

    func loadLeaderboardTypes() {
        leaderboardsVariable.value = .loading
        loadLeaderboards()
    }

    private func loadLeaderboards() {
        repository.observeLeaderboards()
                .subscribe { event in
                    switch event {
                    case .next(let types):
                        self.leaderboardsVariable.value = types.isEmpty ? .empty : .loaded(types)
                    case .error(let error):
                        self.leaderboardsVariable.value = .error(error)
                    case .completed:
                        break
                    }
                }
                .disposed(by: disposeBag)
    }
    
    func observeLeaderboardTypes() -> Observable<State<[LeaderboardType]>> {
        return leaderboardsVariable.asObservable().do(onNext: {
            if case .loaded(let data) = $0 {
                self.data = data
            } else if case .empty = $0 {
                self.data = []
            }
        })
    }
}