//
// Created by Vlad Gorbunov on 06/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import RxSwift

protocol LeaderboardListNavigationDelegate: class {
    func didSelect(type: LeaderboardType)
    func didSelectProfile()
}

class LeaderboardListViewController: BaseListViewController {
    private let viewModel: LeaderboardListViewModel
    private let profileViewModel: ProfileViewModel

    weak var navigationDelegate: LeaderboardListNavigationDelegate?


    init(viewModel: LeaderboardListViewModel, profileViewModel: ProfileViewModel) {
        self.viewModel = viewModel
        self.profileViewModel = profileViewModel
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadLeaderboardTypes()

        viewModel.observeLeaderboardTypes()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { state in
                    self.activityIndicator.ext_setAnimating(state == .loading)
                    self.refreshControl?.ext_setRefreshing(state == .reloading)

                    switch state {
                        case .loaded: self.tableView.backgroundView = nil
                        case .empty: self.handleEmpty(for: L10n.Result.Empty.leaderboards)
                        case .error(let error): self.handle(error: error)
                        default: break
                    }
                    self.tableView.reloadData()
                })
                .disposed(by: disposeBag)

        profileViewModel.observeUser()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { state in
                    if case .loaded(let user) = state, let pictureUrl = user.profilePictureUrl {
                        let profileButton = self.profileAccessoryView(for: pictureUrl, action: #selector(self.didSelectProfile))
                        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: profileButton)
                    }
                })
                .disposed(by: disposeBag)
    }

    override func handleUnauthorized() {
        profileViewModel.logout()
    }

    @objc private func onRefreshControl() {
        viewModel.reloadLeaderboardTypes()
    }

    @objc private func didSelectProfile() {
        navigationDelegate?.didSelectProfile()
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationDelegate?.didSelect(type: viewModel.data[indexPath.row])
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LeaderboardTypeCell.Identifiers.leaderboardType, for: indexPath) as! LeaderboardTypeCell
        cell.leaderboardType = viewModel.data[indexPath.row]
        return cell
    }

    override func loadView() {
        super.loadView()
        tableView.register(LeaderboardTypeCell.self, forCellReuseIdentifier: LeaderboardTypeCell.Identifiers.leaderboardType)

        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor(named: .tabSelected)
        refresh.backgroundColor = UIColor(named: .lightGrey)
        refresh.addTarget(self, action: #selector(onRefreshControl), for: .valueChanged)
        refreshControl = refresh

        navigationItem.title = L10n.Navigation.leaderboards

        let profileButton = profileAccessoryView(action: #selector(didSelectProfile))
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: profileButton)
    }
}