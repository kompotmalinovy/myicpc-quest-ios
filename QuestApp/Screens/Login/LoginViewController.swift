//
//  LoginViewController.swift
//  myicpc-quest
//
//  Created by Vlad Gorbunov on 05/03/2018.
//  Copyright © 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import SnapKit

protocol LoginNavigationDelegate: class {
    func didSelectLogin()
}

class LoginViewController: UIViewController {
    
    weak var navigator: LoginNavigationDelegate?

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @objc private func onLoginClick(sender: UIButton) {
        navigator?.didSelectLogin()
    }

    override func loadView() {
        super.loadView()
        let contentView = UIView()
        view.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let icpcBkg = UIImageView(image: Asset.icpcCrowd2.image)
        icpcBkg.contentMode = .scaleAspectFill
        contentView.addSubview(icpcBkg)
        icpcBkg.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let overlay = UIView()
        overlay.backgroundColor = UIColor(named: .overlayBlue).withAlphaComponent(0.5)
        icpcBkg.addSubview(overlay)
        overlay.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let contentStack = UIStackView()
        contentStack.axis = .vertical
        contentStack.spacing = 30
        contentView.addSubview(contentStack)
        contentStack.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(40)
            make.centerY.equalToSuperview()
        }

        let questLogo = UIImageView()
        questLogo.image = UIImage(asset: Asset.icpcLogoSmall)
        questLogo.contentMode = .scaleAspectFit
        contentStack.addArrangedSubview(questLogo)
        questLogo.snp.makeConstraints { make in
            make.height.equalTo(300)
            make.centerX.equalToSuperview()
        }
        
        let loginBtn = UIButton(type: .system)
        loginBtn.layer.cornerRadius = 6
        loginBtn.clipsToBounds = true
        loginBtn.backgroundColor = UIColor(named: .btnGreen)
        loginBtn.setTitle(L10n.General.loginText, for: .normal)
        loginBtn.setTitleColor(UIColor.white, for: .normal)
        loginBtn.setBackgroundImage(UIColor(named: .btnGreen).image(), for: .normal)
        loginBtn.setBackgroundImage(UIColor(named: .btnGreen).darkened().image(), for: .highlighted)
        loginBtn.addTarget(self, action: #selector(onLoginClick), for: .touchUpInside)
        contentStack.addArrangedSubview(loginBtn)
        loginBtn.snp.makeConstraints { make in
            make.height.equalTo(40)
        }
    }
}
