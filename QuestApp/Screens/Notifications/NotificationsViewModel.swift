//
// Created by Vlad Gorbunov on 21/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import RxSwift

enum NotificationViewMode {
    case timeline
    case whatsHappeningNow
    case accepted(Challenge)
    case pending(Challenge)
    case rejected(Challenge)
}

class NotificationsViewModel: ViewModel {
    private let feedSubject: BehaviorSubject<State<[Notification]>> = BehaviorSubject(value: State.empty)
    private let repository: NotificationRepositoring
    let mode: NotificationViewMode
    var data: [Notification] = []

    init(mode: NotificationViewMode, repository: NotificationRepositoring) {
        self.mode = mode
        self.repository = repository
        super.init()

        if case .timeline = mode {
            repository.observeSilentReload()
                .subscribe(onNext: { if $0 { self.requestFeedUpdate() }})
                .disposed(by: disposeBag)
        }
    }

    func loadFeed() {
        feedSubject.onNext(.loading)
        requestFeedUpdate()
    }

    func reloadFeed() {
        feedSubject.onNext(.reloading)
        requestFeedUpdate()
    }

    private func requestFeedUpdate() {
        getSource(mode: mode)
            .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribe { event in
                switch event {
                    case .next(let submissions):
                        self.feedSubject.onNext(submissions.isEmpty ? .empty : .loaded(submissions))
                    case .error(let error):
                        self.feedSubject.onNext(.error(error))
                    case .completed:
                        break
                }
            }
            .disposed(by: disposeBag)
    }
    
    private func getSource(mode: NotificationViewMode) -> Observable<[Notification]> {
        switch mode {
            case .whatsHappeningNow: return repository.observeFeed()
            case .timeline: return repository.observeTimeline()
            case .accepted(let challenge): return repository.observeAcceptedSubmissions(hashtagSuffix: challenge.hashtagSuffix)
            case .pending(let challenge): return repository.observePendingSubmissions(hashtagSuffix: challenge.hashtagSuffix)
            case .rejected(let challenge): return repository.observeRejectedSubmissions(hashtagSuffix: challenge.hashtagSuffix)
        }
    }

    func observeFeed() -> Observable<State<[Notification]>> {
        return feedSubject.do(onNext: {
            if case .loaded(let data) = $0 {
                self.data = data
            } else if case .empty = $0 {
                self.data = []
            }
        })
    }
}