//
// Created by Vlad Gorbunov on 12/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit

class ModalNotificationsViewController: UIPageViewController {
    private weak var segmentControl: UISegmentedControl!
    private let factory: NotificationsViewControllerFactory
    private var segmentedControllers: [NotificationsViewController] = []
    private let challenge: Challenge
    private var prevSegmentIndex: Int = 0


    init(challenge: Challenge, factory: @escaping NotificationsViewControllerFactory) {
        self.factory = factory
        self.challenge = challenge
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func loadView() {
        super.loadView()
        let segmentView = UISegmentedControl(items: [L10n.Navigation.Submissions.accepted, L10n.Navigation.Submissions.pending, L10n.Navigation.Submissions.rejected])
        segmentView.sizeToFit()
        segmentView.tintColor = UIColor(named: .appTint)
        segmentView.addTarget(self, action: #selector(onSegmentControl), for: .valueChanged)
        navigationItem.titleView = segmentView
        segmentControl = segmentView

        let dismissButton = UIBarButtonItem(image: Asset.icClose.image, style: .done, target: self, action: #selector(onDone))
        dismissButton.tintColor = UIColor(named: .appTint)
        self.navigationItem.leftBarButtonItem = dismissButton
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        segmentControl.selectedSegmentIndex = 0
        segmentedControllers = [factory(self, .accepted(challenge)), factory(self, .pending(challenge)), factory(self, .rejected(challenge))]
        onSegmentControl(sender: segmentControl)
    }

    @objc private func onSegmentControl(sender: UISegmentedControl) {
        let index = segmentControl.selectedSegmentIndex
        setViewControllers([segmentedControllers[index]], direction: index > prevSegmentIndex ? .forward : .reverse, animated: true)
        prevSegmentIndex = index
    }

    @objc private func onDone() {
        self.dismiss(animated: true)
    }
}

// we only need this since our factory for notifications vc awaits nav delegate as parameter
extension ModalNotificationsViewController: NotificationListNavigationDelegate {
    func didSelect(submission: Notification) {
        // action placeholder
        // don't know if there would be something to display
        // in "detail view"
    }

    func didSelectProfile() {}

    func didSelectPost() {}
}