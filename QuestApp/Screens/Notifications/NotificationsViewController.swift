//
//  NotificationsViewController.swift
//  QuestApp
//
//  Created by Vlad Gorbunov on 14/03/2018.
//  Copyright © 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import RxSwift
import SKPhotoBrowser
import AVKit
import AVFoundation

protocol NotificationListNavigationDelegate: class {
    func didSelect(submission: Notification)
    func didSelectProfile()
    func didSelectPost()
}

class NotificationsViewController: BaseListViewController {
    private let viewModel: NotificationsViewModel
    private let profileViewModel: ProfileViewModel
    weak var navigationDelegate: NotificationListNavigationDelegate?

    init(viewModel: NotificationsViewModel, profileViewModel: ProfileViewModel) {
        self.viewModel = viewModel
        self.profileViewModel = profileViewModel
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK: lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadFeed()
        viewModel.observeFeed()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { state in
                    self.activityIndicator.ext_setAnimating(state == .loading)
                    self.refreshControl?.ext_setRefreshing(state == .reloading)
                    switch state {
                        case .empty: self.handleEmpty(for: L10n.Result.Empty.feed)
                        case .loaded: self.tableView.backgroundView = nil
                        case .error(let error): self.handle(error: error)
                    default: break
                    }
                    self.tableView.reloadData()
                })
                .disposed(by: disposeBag)

        // Swift pattern matching is the only option, since swift can't compare enums which may have associated values unless they implement Equatable
        switch viewModel.mode {
            case .timeline:
                let sendButton = UIBarButtonItem(image: Asset.icSend2.image, style: .plain, target: self, action: #selector(didSelectPost))
                sendButton.tintColor = UIColor(named: .appTint)
                self.navigationItem.rightBarButtonItem = sendButton
                fallthrough
            case .timeline, .whatsHappeningNow:
                profileViewModel.observeUser()
                        .observeOn(MainScheduler.instance)
                        .subscribe(onNext: { state in
                            if case .loaded(let user) = state, let pictureUrl = user.profilePictureUrl {
                                let profileButton = self.profileAccessoryView(for: pictureUrl, action: #selector(self.didSelectProfile))
                                self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: profileButton)
                            }
                        })
                        .disposed(by: disposeBag)
            default:
                break
        }
    }


    // MARK: BaseListViewController overrides

    override func handleUnauthorized() {
        profileViewModel.logout()
    }


    // MARK: private methods

    @objc private func didSelectProfile() {
        navigationDelegate?.didSelectProfile()
    }

    @objc private func didSelectPost() {
        navigationDelegate?.didSelectPost()
    }

    private func showPhoto(imageUrl: URL) {
        let pic = SKPhoto.photoWithImageURL(imageUrl.absoluteString)
        SKPhotoBrowserOptions.displayAction = false
        let browser = SKPhotoBrowser(photos: [pic])
        self.present(browser, animated: true, completion: nil)
    }

    private func showVideo(videoUrl: URL) {
        let playerView = AVPlayer(url: videoUrl)
        let playerController = AVPlayerViewController()
        playerController.player = playerView
        self.present(playerController, animated: true)
        playerController.player?.play()
    }

    private func reuseIdentifier(for notification: Notification) -> String {
        if notification.notificationType != Constants.NOTIFICATION_QUEST_APP && notification.notificationType != Constants.NOTIFICATION_TWITTER {
            return SystemNotificationCell.Identifiers.systemNotification
        }

        let hasPhoto = notification.imageUrl != nil
        let hasVideo = notification.videoUrl != nil

        if hasPhoto && hasVideo {
            return NotificationCell.Identifiers.withTextImageVideo
        } else if hasPhoto {
            return NotificationCell.Identifiers.withTextImage
        } else if hasVideo {
            return NotificationCell.Identifiers.withTextVideo
        } else {
            return NotificationCell.Identifiers.withText
        }
    }

    @objc private func onRefreshControl() {
        viewModel.reloadFeed()
    }


    // MARK: UITableViewController delegate methods

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationDelegate?.didSelect(submission: viewModel.data[indexPath.row])
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if viewModel.data[indexPath.row].notificationType != Constants.NOTIFICATION_QUEST_APP && viewModel.data[indexPath.row].notificationType != Constants.NOTIFICATION_TWITTER {
            let cell: SystemNotificationCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier(for: viewModel.data[indexPath.row]), for: indexPath) as! SystemNotificationCell
            cell.selectionStyle = .none
            cell.notification = viewModel.data[indexPath.row]
            return cell
        } else {
            let cell: NotificationCell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier(for: viewModel.data[indexPath.row]), for: indexPath) as! NotificationCell
            cell.selectionStyle = .none
            cell.notification = viewModel.data[indexPath.row]
            cell.didSelectImage = { [weak self] url in
                self?.showPhoto(imageUrl: url)
            }
            cell.didSelectVideo = { [weak self] url in
                self?.showVideo(videoUrl: url)
            }
            return cell
        }
    }

    // MARK: view definition
    override func loadView() {
        super.loadView()
        tableView.register(NotificationCell.self, forCellReuseIdentifier: NotificationCell.Identifiers.withText)
        tableView.register(NotificationCell.self, forCellReuseIdentifier: NotificationCell.Identifiers.withTextImage)
        tableView.register(NotificationCell.self, forCellReuseIdentifier: NotificationCell.Identifiers.withTextVideo)
        tableView.register(NotificationCell.self, forCellReuseIdentifier: NotificationCell.Identifiers.withTextImageVideo)
        tableView.register(SystemNotificationCell.self, forCellReuseIdentifier: SystemNotificationCell.Identifiers.systemNotification)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300

        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor(named: .appTint)
        refresh.backgroundColor = UIColor(named: .lightGrey)
        refresh.addTarget(self, action: #selector(onRefreshControl), for: .valueChanged)
        refreshControl = refresh

        navigationItem.title = {
            switch viewModel.mode {
                case .whatsHappeningNow: return L10n.Navigation.Submissions.whatsHappeningNow
                case .timeline: return L10n.Navigation.Submissions.timeline
                default: return ""
            }
        }()

        // Swift switch pattern matching is the only option, since swift can't compare enums which may have associated values, unless they implement Equatable
        switch viewModel.mode {
            case .whatsHappeningNow, .timeline:
                let profileButton = profileAccessoryView(action: #selector(didSelectProfile))
                navigationItem.leftBarButtonItem = UIBarButtonItem(customView: profileButton)
            default:
                break
        }
    }
}
