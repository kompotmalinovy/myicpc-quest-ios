//
// Created by Vlad Gorbunov on 14/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import RxSwift

class ProfileViewModel: ViewModel {

    private let userVariable = Variable(State<User>.idle)
    private let repository: UserRepositoring
    private let userManager: UserManaging
    private let appSettings: AppSettingsManaging

    init(userManager: UserManaging, appSettings: AppSettingsManaging, repository: UserRepositoring) {
        self.repository = repository
        self.userManager = userManager
        self.appSettings = appSettings
        super.init()

        if let user = userManager.user {
            userVariable.value = .loaded(user)
        } else {
            loadUser()
        }
    }

    var isContextSettingEnabled: Bool {
        get { return appSettings.contextPermissionGranted }
        set { appSettings.contextPermissionGranted = newValue }
    }

    func loadUser() {
        userVariable.value = .loading
        repository.observeUser()
                .subscribeOn(ConcurrentDispatchQueueScheduler(qos: .background))
                .subscribe { event in
                    switch event {
                        case .next(let user):
                            self.userManager.user = user
                            self.userVariable.value = .loaded(user)
                        case .error(let error):
                            self.userVariable.value = .error(error)
                        case .completed:
                            break
                    }
                }
                .disposed(by: disposeBag)
    }

    func logout() {
        userManager.logout()
    }

    func observeUser() -> Observable<State<User>> {
        return userVariable.asObservable()
    }
}