//
// Created by Vlad Gorbunov on 08/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import AlamofireImage
import Foundation
import SafariServices

class ProfileViewController: UIViewController {

    private weak var profileImage: UIImageView!
    private weak var nameLabel: UILabel!
    private weak var usernameLabel: UILabel!
    private weak var activityIndicator: UIActivityIndicatorView!
    private weak var contextOnSwitch: UISwitch!

    private let disposeBag = DisposeBag()
    private let viewModel: ProfileViewModel


    init(viewModel: ProfileViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK: lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.observeUser()
                .observeOn(MainScheduler.instance)
                .subscribe(onNext: { state in
                    self.activityIndicator.ext_setAnimating(state == .loading)
                    if case .loaded(let user) = state {
                        self.profileImage.isHidden = false
                        if let url = user.profilePictureUrl {
                            self.profileImage.af_setImage(withURL: url, placeholderImage: Asset.icProfilePlaceholder.image)
                        }
                        self.nameLabel.text = L10n.Profile.name(user.name, user.surname)
                        self.usernameLabel.text = L10n.Profile.username(user.twitterUsername)
                    }
                    if case .error(let error) = state, case AuthError.unauthorized = error {
                        self.viewModel.logout()
                    }
                })
                .disposed(by: disposeBag)

        contextOnSwitch.setOn(viewModel.isContextSettingEnabled, animated: false)

        contextOnSwitch.rx.value
                .skip(1)
                .subscribe(onNext: { value in
                    self.viewModel.isContextSettingEnabled = value
                })
                .disposed(by: disposeBag)
    }


    // MARK: private methods

    @objc private func didSelectRules() {
        let controller = SFSafariViewController(url: URL(string: Environment.External.rulesLink)!)
        navigationController?.present(controller, animated: true)
    }

    @objc private func didSelectLogout() {
        let dialog = UIAlertController(title: L10n.General.Logout.title, message: L10n.General.Logout.message, preferredStyle: .alert)
        dialog.addAction(UIAlertAction(title: L10n.General.Action.logout, style: .default, handler: { _ in
            let controller = SFSafariViewController(url: URL(string: Environment.Auth.logoutEndpoint)!)
            self.navigationController?.present(controller, animated: true, completion: {
                DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + 3) {
                    self.viewModel.logout()
                }
            })
        }))
        dialog.addAction(UIAlertAction(title: L10n.General.Action.cancel, style: .cancel, handler: nil))
        self.present(dialog, animated: true)
    }

    @objc private func onDone() {
        self.dismiss(animated: true)
    }


    // MARK: view definition
    override func loadView() {
        super.loadView()
        navigationItem.title = L10n.Navigation.profile

        let dismissBtn = UIBarButtonItem(image: Asset.icClose.image, style: .plain, target: self, action: #selector(onDone))
        dismissBtn.tintColor = UIColor(named: .appTint)
        self.navigationItem.leftBarButtonItem = dismissBtn

        let scrollView = UIScrollView()
        scrollView.delaysContentTouches = false
        scrollView.backgroundColor = .white
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let contentView = UIView()
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.top.leading.trailing.width.equalToSuperview()
            make.bottom.equalToSuperview().inset(20)
        }

        let profileBackground = UIImageView(image: Asset.icpcCrowd2.image)
        profileBackground.contentMode = .scaleAspectFill
        contentView.addSubview(profileBackground)
        profileBackground.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalTo(scrollView.snp.width).multipliedBy(3.0 / 4.0)
        }

        let overlay = UIView()
        overlay.backgroundColor = UIColor(named: .overlayBlue).withAlphaComponent(0.5)
        profileBackground.addSubview(overlay)
        overlay.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        let username = UILabel()
        username.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        username.textColor = .white
        profileBackground.addSubview(username)
        username.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(12)
        }
        self.usernameLabel = username

        let name = UILabel()
        name.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        name.textColor = .white
        profileBackground.addSubview(name)
        name.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(username.snp.top)
        }
        self.nameLabel = name

        let activity = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activity.hidesWhenStopped = true
        profileBackground.addSubview(activity)
        activity.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        self.activityIndicator = activity

        let profileImage = UIImageView(image: Asset.icProfilePlaceholder.image)
        profileImage.layer.cornerRadius = 64
        profileImage.layer.borderWidth = 2
        profileImage.layer.borderColor = UIColor(named: .btnGreen).cgColor
        profileImage.clipsToBounds = true
        profileImage.contentMode = .scaleAspectFill
        profileBackground.addSubview(profileImage)
        profileImage.isHidden = true
        profileImage.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.height.equalTo(128)
            make.bottom.equalTo(name.snp.top).inset(-6)
        }
        self.profileImage = profileImage

        /*
           this is a pretty bad way to do settings, ideally we would have a tableview with different cells
           and cells for settings (ideally settings bundle too), but given that we only have 1 setting this will work...
        */
        let settingContainer = UIView()
        contentView.addSubview(settingContainer)
        settingContainer.snp.makeConstraints { make in
            make.top.equalTo(profileBackground.snp.bottom).offset(40)
            make.leading.trailing.equalToSuperview().inset(12)
            make.width.equalToSuperview().inset(12)
            make.height.equalTo(80)
        }

        let separatorTop = UIView()
        separatorTop.backgroundColor = UIColor(named: .textTitle).withAlphaComponent(0.3)
        settingContainer.addSubview(separatorTop)
        separatorTop.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }

        let separatorBottom = UIView()
        separatorBottom.backgroundColor = UIColor(named: .textTitle).withAlphaComponent(0.3)
        settingContainer.addSubview(separatorBottom)
        separatorBottom.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }

        let permissionSwitch = UISwitch()
        permissionSwitch.onTintColor = UIColor(named: .appTint)
        settingContainer.addSubview(permissionSwitch)
        permissionSwitch.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        self.contextOnSwitch = permissionSwitch

        let contextPermission = UILabel()
        contextPermission.font = UIFont.systemFont(ofSize: 16.0, weight: .medium)
        contextPermission.textColor = UIColor(named: .textTitle).withAlphaComponent(0.8)
        contextPermission.textAlignment = .center
        contextPermission.numberOfLines = 2
        contextPermission.text = L10n.Profile.Settings.context
        settingContainer.addSubview(contextPermission)
        contextPermission.snp.makeConstraints { make in
            make.leading.centerY.equalToSuperview()
            make.trailing.lessThanOrEqualTo(permissionSwitch.snp.leading)
        }

        let aboutBtn = UIButton(type: .system)
        aboutBtn.layer.cornerRadius = 6
        aboutBtn.clipsToBounds = true
        aboutBtn.setTitle(L10n.Profile.rules, for: .normal)
        aboutBtn.setTitleColor(UIColor.white, for: .normal)
        aboutBtn.backgroundColor = UIColor(named: .btnGreen)
        aboutBtn.setBackgroundImage(UIColor(named: .btnGreen).image(), for: .normal)
        aboutBtn.setBackgroundImage(UIColor(named: .btnGreen).darkened().image(), for: .highlighted)
        aboutBtn.addTarget(self, action: #selector(didSelectRules), for: .touchUpInside)
        contentView.addSubview(aboutBtn)
        aboutBtn.snp.makeConstraints { make in
            make.top.equalTo(settingContainer.snp.bottom).offset(30)
            make.leading.trailing.equalToSuperview().inset(12)
            make.width.equalToSuperview().inset(12)
            make.height.equalTo(40)
        }

        let logout = UIButton(type: .system)
        logout.layer.cornerRadius = 6
        logout.clipsToBounds = true
        logout.backgroundColor = UIColor(named: .btnBlue)
        logout.setTitle(L10n.Profile.logout, for: .normal)
        logout.setTitleColor(UIColor.white, for: .normal)
        logout.setBackgroundImage(UIColor(named: .btnBlue).image(), for: .normal)
        logout.setBackgroundImage(UIColor(named: .btnBlue).darkened().image(), for: .highlighted)
        logout.addTarget(self, action: #selector(didSelectLogout), for: .touchUpInside)
        contentView.addSubview(logout)
        logout.snp.makeConstraints { make in
            make.top.equalTo(aboutBtn.snp.bottom).offset(10)
            make.width.equalToSuperview().inset(12)
            make.leading.trailing.equalToSuperview().inset(12)
            make.bottom.lessThanOrEqualToSuperview()
            make.height.equalTo(40)
        }
    }
}