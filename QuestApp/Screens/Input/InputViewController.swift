//
// Created by Vlad Gorbunov on 19/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxSwift
import AlamofireImage
import Gallery
import RxCocoa

class InputViewController: UIViewController, UITextViewDelegate, GalleryControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    private weak var mediaCollectionView: UICollectionView!

    private weak var profileIcon: UIImageView!
    private weak var messageTextView: UITextView!
    private let textViewHintColor = UIColor(named: .textTitle).withAlphaComponent(0.5)
    private weak var bottomAction: UIView!

    private weak var hashtagLabel: UILabel!
    private weak var addPhotoButton: UIButton!
    private weak var addVideoButton: UIButton!
    private weak var loadingAlert: UIAlertController!
    private var bottomConstraint: NSLayoutConstraint?
    private let disposeBag = DisposeBag()

    private let viewModel: InputViewModel
    private let profileViewModel: ProfileViewModel

    init(viewModel: InputViewModel, profileViewModel: ProfileViewModel) {
        self.viewModel = viewModel
        self.profileViewModel = profileViewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    // MARK: lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()

        hashtagLabel.text = {
            switch viewModel.type {
                case .timeline: return L10n.Notification.hashtag(Environment.Contest.code)
                case .challenge(let challenge): return L10n.Notification.hashtag(Environment.Contest.code) + " " + L10n.Notification.hashtag(challenge.hashtagPrefix + challenge.hashtagSuffix)
            }
        }()

        profileViewModel.observeUser()
                .observeOn(MainScheduler.asyncInstance)
                .subscribe(onNext: { state in
                    if case .loaded(let user) = state, let imageUrl = user.profilePictureUrl {
                        self.profileIcon.af_setImage(withURL: imageUrl)
                    }
                })
                .disposed(by: disposeBag)

        viewModel.observeMedia()
                .subscribeOn(MainScheduler.asyncInstance)
                .subscribe(onNext: { _ in self.mediaCollectionView.reloadData()})
                .disposed(by: disposeBag)

        viewModel.observeInputState()
                .subscribeOn(MainScheduler.asyncInstance)
                .subscribe(onNext: { state in
                    self.setRightAccessory(for: state)
                    if case .uploaded = state { self.dismiss(animated: true) }
                    if case .error = state { self.displayErrorAlert() }
                })
                .disposed(by: disposeBag)

        let stateIsLoading = viewModel.observeInputState()
                .map { $0 == .uploading }

        let maxPhotoNotReached = viewModel.observeMedia()
                .map { $0.filter { if case .photo = $0 { return true } else { return false }}}
                .map { $0.count < Constants.IMAGE_SEND_LIMIT }

        let isPhotoEnabled = Observable.combineLatest(maxPhotoNotReached, stateIsLoading) { $0 && !$1 }
        isPhotoEnabled.bind(to: addPhotoButton.rx.isEnabled).disposed(by: disposeBag)

        let maxVideoNotReached = viewModel.observeMedia()
                .map { $0.filter { if case .video = $0 { return true } else { return false }}}
                .map { $0.count < Constants.VIDEO_SEND_LIMIT }

        let isVideoEnabled = Observable.combineLatest(maxVideoNotReached, stateIsLoading) { $0 && !$1}
        isVideoEnabled.bind(to: addVideoButton.rx.isEnabled).disposed(by: disposeBag)

        messageTextView.rx.text.orEmpty.bind(to: viewModel.message).disposed(by: disposeBag)
        keyboardHeight().map { -$0 }.bind(to: bottomConstraint!.rx.constant).disposed(by: disposeBag)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        messageTextView.resignFirstResponder()
    }


    // MARK: private functions

    @objc private func endEditing() {
        view.endEditing(true)
    }

    @objc private func onDone() {
        dismiss(animated: true)
    }

    @objc private func didSelectChoosePhoto() {
        Config.tabsToShow = [.cameraTab, .imageTab]
        Config.Camera.imageLimit = Constants.IMAGE_SEND_LIMIT
        let controller = GalleryController()
        controller.delegate = self
        navigationController?.present(controller, animated: true)
    }

    @objc private func didSelectChooseVideo() {
        Config.tabsToShow = [.videoTab]
        Config.Camera.imageLimit = Constants.VIDEO_SEND_LIMIT
        let controller = GalleryController()
        controller.delegate = self
        navigationController?.present(controller, animated: true)
    }

    @objc private func didSelectSend() {
        viewModel.send()
    }

    private func setRightAccessory(for inputState: InputState) {
        if case .uploading = inputState {
            let ai = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            ai.hidesWhenStopped = true
            ai.color = UIColor(named: .appTint)
            ai.startAnimating()
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: ai)
        } else {
            let sendBtn = UIBarButtonItem(image: Asset.icSend2.image, style: .done, target: self, action: #selector(didSelectSend))
            sendBtn.tintColor = UIColor(named: .appTint)
            navigationItem.rightBarButtonItem = sendBtn

            let isTextPresent = viewModel.message.asObservable().map { !$0.isEmpty && self.messageTextView.textColor != self.textViewHintColor }
            let isMediaPresent = viewModel.observeMedia().map { !$0.isEmpty }
            let isLoading = viewModel.observeInputState().map { $0 == .uploading }
            let isPostEnabled = Observable.combineLatest(isTextPresent, isMediaPresent, isLoading) { ($0 || $1) && !$2 }

            isPostEnabled.bind(to: navigationItem.rightBarButtonItem!.rx.isEnabled).disposed(by: disposeBag)
        }
    }

    private func displayErrorAlert() {
        let dialog = UIAlertController(title: L10n.Input.Error.title, message: L10n.Input.Error.message, preferredStyle: .alert)
        dialog.addAction(UIAlertAction(title: L10n.General.Action.accept, style: .default, handler: nil))
        self.present(dialog, animated: true)
    }


    // MARK: GalleryControllerDelegate methods

    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        viewModel.update(with: [.video(video)])
        controller.dismiss(animated: true)
    }

    func galleryController(_ controller: GalleryController, didSelectImages images: [Gallery.Image]) {
        viewModel.update(with: images.map { MediaType.photo($0) })
        controller.dismiss(animated: true)
    }

    func galleryController(_ controller: GalleryController, requestLightbox images: [Gallery.Image]) {
        controller.dismiss(animated: true)
    }

    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true)
    }


    // MARK: UITextViewDelegate methods

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText:String = textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)

        if updatedText.isEmpty {
            textView.text = L10n.Input.Message.hint
            textView.textColor = textViewHintColor
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            return false
        } else if textView.textColor == textViewHintColor && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor(named: .textTitle)
        }
        return true
    }

    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.view.window != nil {
            if textView.textColor == textViewHintColor {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
    }


    // MARK: UICollectionViewDelegate & UICollectionViewDataSource methods

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.media.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let media = viewModel.media[indexPath.row]

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MediaCollectionViewCell.Identifiers.image, for: indexPath) as! MediaCollectionViewCell
        cell.remove = { [weak self] in
            self?.viewModel.remove(mediaItem: media)
        }
        cell.media = media
        return cell
    }


    // MARK: view definition

    override func loadView() {
        super.loadView()

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(endEditing))
        view.addGestureRecognizer(tapGestureRecognizer)
        view.backgroundColor = .white

        let dismissBtn = UIBarButtonItem(image: Asset.icClose.image, style: .plain, target: self, action: #selector(onDone))
        dismissBtn.tintColor = UIColor(named: .appTint)
        self.navigationItem.leftBarButtonItem = dismissBtn

        let scrollView = UIScrollView()
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 0, left: 0, bottom: 50, right: 0))
        }

        let contentView = UIView()
        scrollView.addSubview(contentView)
        contentView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(12)
            make.top.bottom.equalToSuperview().inset(20)
            make.width.equalToSuperview().inset(12)
        }

        let profile = UIImageView(image: Asset.icProfilePlaceholderSmall.image)
        profile.layer.cornerRadius = 24
        profile.clipsToBounds = true
        profile.contentMode = .scaleAspectFill
        contentView.addSubview(profile)
        profile.snp.makeConstraints { make in
            make.top.leading.equalToSuperview()
            make.width.height.equalTo(48)
        }
        self.profileIcon = profile

        let textView = UITextView()
        textView.text = L10n.Input.Message.hint
        textView.textColor = UIColor(named: .textTitle).withAlphaComponent(0.5)
        textView.becomeFirstResponder()
        textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        textView.font = UIFont.systemFont(ofSize: 20)
        textView.isEditable = true
        textView.delegate = self
        textView.autocapitalizationType = .sentences
        textView.isScrollEnabled = false
        textView.textContainer.lineFragmentPadding = 0
        textView.layoutManager.usesFontLeading = false
        contentView.addSubview(textView)
        textView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(4)
            make.trailing.equalToSuperview()
            make.leading.equalTo(profile.snp.trailing).offset(10)
            make.height.greaterThanOrEqualTo(50)
            make.bottom.lessThanOrEqualToSuperview()
        }
        self.messageTextView = textView

        let hashtagLabel = UILabel()
        hashtagLabel.numberOfLines = 0
        hashtagLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        hashtagLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.9)
        contentView.addSubview(hashtagLabel)
        hashtagLabel.snp.makeConstraints { make in
            make.top.equalTo(textView.snp.bottom).offset(10)
            make.leading.equalTo(profile.snp.trailing).offset(10)
            make.trailing.equalToSuperview().inset(12)
        }
        self.hashtagLabel = hashtagLabel

        let bottomAction = UIView()
        bottomAction.backgroundColor = .white
        view.addSubview(bottomAction)
        bottomAction.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(50)
        }
        self.bottomAction = bottomAction

        bottomConstraint = NSLayoutConstraint(item: bottomAction, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)

        let separator = UIView()
        separator.backgroundColor = UIColor(named: .textTitle).withAlphaComponent(0.3)
        bottomAction.addSubview(separator)
        separator.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }

        let addImage = UIButton()
        addImage.setImage(Asset.icPickImage2.image.withRenderingMode(.alwaysTemplate), for: .normal)
        addImage.imageView?.tintColor = UIColor(named: .appTint)
        addImage.imageView?.contentMode = .scaleAspectFit
        addImage.addTarget(self, action: #selector(didSelectChoosePhoto), for: .touchUpInside)
        bottomAction.addSubview(addImage)
        addImage.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(12)
            make.width.height.equalTo(30)
            make.centerY.equalToSuperview()
        }
        self.addPhotoButton = addImage

        let addVideo = UIButton()
        addVideo.setImage(Asset.icPickVideo.image.withRenderingMode(.alwaysTemplate), for: .normal)
        addVideo.imageView?.tintColor = UIColor(named: .appTint)
        addVideo.imageView?.contentMode = .scaleAspectFit
        addVideo.addTarget(self, action: #selector(didSelectChooseVideo), for: .touchUpInside)
        bottomAction.addSubview(addVideo)
        addVideo.snp.makeConstraints { make in
            make.leading.equalTo(addImage.snp.trailing).offset(12)
            make.width.height.equalTo(30)
            make.centerY.equalToSuperview()
        }
        self.addVideoButton = addVideo

        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 12 + 48 + 12, bottom: 0, right: 12)
        flowLayout.scrollDirection = .horizontal

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .white
        collectionView.alwaysBounceHorizontal = true
        collectionView.showsHorizontalScrollIndicator = false
        scrollView.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(hashtagLabel.snp.bottom).offset(20)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(250)
            make.bottom.lessThanOrEqualToSuperview()
        }

        collectionView.register(MediaCollectionViewCell.self, forCellWithReuseIdentifier: MediaCollectionViewCell.Identifiers.image)
        collectionView.register(MediaCollectionViewCell.self, forCellWithReuseIdentifier: MediaCollectionViewCell.Identifiers.video)
        self.mediaCollectionView = collectionView
    }
}
