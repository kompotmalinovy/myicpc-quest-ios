//
// Created by Vlad Gorbunov on 19/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Gallery
import Photos

enum InputType {
    case timeline
    case challenge(Challenge)
}

enum MediaType {
    case photo(Gallery.Image)
    case video(Gallery.Video)
}

class InputViewModel: ViewModel {

    private let repository: NotificationRepositoring
    private var mediaVariable = Variable<[MediaType]>([])
    private let inputState = Variable<InputState>(.idle)

    let type: InputType
    let message = Variable<String>("")
    var media: [MediaType] {
        get { return mediaVariable.value }
    }

    var hashtags: [String] {
        get {
            if case .timeline = type {
                return [Environment.Contest.code]
            }
            if case .challenge(let challenge) = type {
                return [Environment.Contest.code, challenge.hashtagPrefix + challenge.hashtagSuffix]
            }
            return [String]()
        }
    }


    init(type: InputType, repository: NotificationRepositoring) {
        self.type = type
        self.repository = repository
    }

    func send() {
        inputState.value = .uploading

        let photoData: [Observable<Data?>] = media
                .compactMap { (media: MediaType) in
                    if case .photo(let image) = media { return image.asset } else { return nil }
                }
                .map { (asset: PHAsset) in
                    asset.ext_toImageObservable().map { UIImageJPEGRepresentation($0, 0.8) }
                }


        let imageDataArrayObservable = photoData.isEmpty ? Observable.just([Data?]()) : Observable.combineLatest(photoData) { $0 }


        let videoData: [Observable<Data?>] = media
                .compactMap { (media: MediaType) in
                    if case .video(let video) = media { return video.asset } else { return nil }
                }
                .map { (asset: PHAsset) in
                    asset.ext_toAVURLAssetObservable().map { try? Data(contentsOf: $0.url) }
                }

        let videoDataArrayObservable = videoData.isEmpty ? Observable.just([Data?]()) : Observable.combineLatest(videoData) { $0 }


        let submissionSource = repository.sendSubmission(
                hashtagObservable: Observable.just("|\(hashtags.joined(separator: "|"))|"),
                messageObservable: message.asObservable().take(1),
                imagesObservable: imageDataArrayObservable.map { $0.filter { $0 != nil }.map { $0! }},
                videosObservable: videoDataArrayObservable.map { $0.filter { $0 != nil }.map { $0! }}
        )

        submissionSource.subscribeOn(MainScheduler.asyncInstance)
                .subscribe { event in
                    switch event {
                        case .next:
                            self.inputState.value = .uploaded
                        case .error(let error):
                            self.inputState.value = .error(error)
                        case .completed:
                            break
                    }
                }
                .disposed(by: disposeBag)
    }
}

extension InputViewModel {
    func observeInputState() -> Observable<InputState> {
        return inputState.asObservable().distinctUntilChanged()
    }

    func observeMedia() -> Observable<[MediaType]> {
        return mediaVariable.asObservable()
    }

    func update(with media: [MediaType]) {
        mediaVariable.value = mediaVariable.value + media
    }

    func remove(mediaItem: MediaType) {
        mediaVariable.value = media.filter { $0 != mediaItem }
    }
}

extension MediaType: Equatable {
    static func ==(lhs: MediaType, rhs: MediaType) -> Bool {
        switch (lhs, rhs) {
        case (.photo(let p1), .photo(let p2)):
            return p1 == p2
        case (.video(let u1), .video(let u2)):
            return u1 == u2
        default:
            return false
        }
    }
}