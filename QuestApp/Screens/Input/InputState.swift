//
// Created by Vlad Gorbunov on 23/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

enum InputState {
    case uploading
    case uploaded
    case error(Error)
    case idle
}

extension InputState: Equatable {
    static func ==(lhs: InputState, rhs: InputState) -> Bool {
        switch (lhs, rhs) {
        case (.uploading, .uploading):
            return true
        case (.uploaded, .uploaded):
            return true
        case (.error, .error):
            return true
        case (.idle, .idle):
            return true
        default:
            return false
        }
    }
}