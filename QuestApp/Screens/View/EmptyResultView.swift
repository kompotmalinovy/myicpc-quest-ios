//
// Created by Vlad Gorbunov on 08/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import SnapKit

class EmptyResultView: UIView {
    //weak var iconImage: UIImageView!
    weak var titleLabel: UILabel!
    weak var descriptionLabel: UILabel!
    weak var hintLabel: UILabel!

    init() {
        super.init(frame: .zero)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupView() {
        backgroundColor = UIColor(named: .warmWhite)

        let contentStack = UIStackView()
        contentStack.axis = .vertical
        contentStack.spacing = 10
        addSubview(contentStack)
        contentStack.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.centerX.centerY.equalToSuperview()
        }

        let titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        titleLabel.text = L10n.Result.Empty.title
        titleLabel.textColor = UIColor(named: .textTitle)
        titleLabel.textAlignment = .center
        contentStack.addArrangedSubview(titleLabel)

        let descriptionLabel = UILabel()
        descriptionLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        descriptionLabel.text = description
        descriptionLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.7)
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .center
        contentStack.addArrangedSubview(descriptionLabel)

        let hint = UILabel()
        hint.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        hint.text = L10n.Result.Empty.hint
        hint.textColor = UIColor(named: .textTitle).withAlphaComponent(0.7)
        hint.numberOfLines = 0
        hint.textAlignment = .center
        contentStack.addArrangedSubview(hint)
    }
}