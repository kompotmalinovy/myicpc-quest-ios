//
// Created by Vlad Gorbunov on 06/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class LeaderboardTypeCell: UITableViewCell {
    enum Identifiers {
        static let leaderboardType = "leaderboard_type"
    }

    private weak var leaderboardIcon: UIImageView!
    private weak var leaderboardTitle: UILabel!

    var leaderboardType: LeaderboardType? {
        didSet {
            guard let type = leaderboardType else { return }

            let asset: ImageAsset = {
                switch type {
                    case .team: return Asset.icRankTeam
                    case .generic(let item): return {
                        switch $0 {
                            case _ where $0.contains(L10n.Role.staff.lowercased()) && $0.count == 1: return Asset.icRankStaff
                            case _ where $0.contains(L10n.Role.contestant.lowercased()) && $0.count == 1: return Asset.icRankContestant
                            case _ where $0.contains(L10n.Role.attendee.lowercased()) && $0.count == 1: return Asset.icRankAttendee
                            case _ where $0.contains(L10n.Role.coach.lowercased()) && $0.count == 1: return Asset.icRankCoach
                            case _ where $0.contains(L10n.Role.cocoach.lowercased()) && $0.count == 1: return Asset.icRankCoach
                            case _ where $0.contains(L10n.Role.reserve.lowercased()) && $0.count == 1: return Asset.icRankReserve
                            default: return Asset.icRankGeneral
                        }
                    }(item.roles.map { $0.lowercased() })
                }
            }()

            leaderboardIcon.image = UIImage(asset: asset)
            leaderboardTitle.text = {
                switch type {
                    case .generic(let item): return item.name
                    case .team: return L10n.Leaderboard.teamLeaderboard
                }
            }()
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let contentStack = UIStackView()
        contentStack.axis = .horizontal
        contentStack.spacing = 15
        contentStack.distribution = .fill
        contentView.addSubview(contentStack)
        contentStack.snp.makeConstraints { make in
            make.edges.equalTo(0).inset(12)
        }

        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        contentStack.addArrangedSubview(imageView)
        imageView.snp.makeConstraints { make in 
            make.width.equalTo(72)
        }
        self.leaderboardIcon = imageView

        let title = UILabel()
        title.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        title.textColor = UIColor(named: .textTitle)
        contentStack.addArrangedSubview(title)
        self.leaderboardTitle = title
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

