//
// Created by Vlad Gorbunov on 26/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Alamofire
import AlamofireImage

class NotificationCell: UITableViewCell {
    
    enum Identifiers {
        static let withText = "item+text"
        static let withTextImage = "item+image"
        static let withTextVideo = "item+video"
        static let withTextImageVideo = "item+image+video"
    }

    private weak var profileImage: UIImageView!
    private weak var usernameLabel: UILabel!
    private weak var authorLabel: UILabel!

    private weak var bodyLabel: UILabel!
    private weak var hashtagLabel: UILabel!
    private weak var photoImage: UIImageView!
    private weak var videoImage: UIImageView!
    private weak var sourceLabel: UILabel!

    var didSelectImage: ((URL) -> ())?
    var didSelectVideo: ((URL) -> ())?

    var notification: Notification? {
        didSet {
            guard let notification = notification else { return }

            guard notification.notificationType == Constants.NOTIFICATION_TWITTER || notification.notificationType == Constants.NOTIFICATION_QUEST_APP else { return }

            adjustConstraints(item: notification)

            authorLabel.text = notification.authorName!
            usernameLabel.text = L10n.Notification.profile(notification.authorUsername!)

            bodyLabel.text = notification.body
            hashtagLabel.text = notification.hashtags?.map { L10n.Notification.hashtag($0) }.joined(separator: " ")

            if let url = notification.profilePictureUrl {
                profileImage.af_setImage(withURL: url, placeholderImage: Asset.icProfilePlaceholder.image)
            } else {
                profileImage.image = Asset.icProfilePlaceholder.image
            }

            if let imageUrl = notification.imageUrl {
                photoImage.af_setImage(withURL: imageUrl)
            }

            if let videoUrl = notification.videoUrl {
                videoImage.ext_loadVideoThumbnail(url: videoUrl)
            }

            let sourceString: String = {
                if notification.notificationType == Constants.NOTIFICATION_QUEST_APP {
                    return L10n.Notification.Source.mobileApp
                } else if notification.notificationType == Constants.NOTIFICATION_TWITTER {
                    return L10n.Notification.Source.twitter
                } else {
                    return L10n.Notification.Source.system
                }
            }()

            sourceLabel.text = L10n.Notification.source(DateFormatters.sharedDayMonthTime.string(from: notification.submitDate), sourceString)
        }
    }

    @objc private func onImageTapped(gestureRecognizer: UITapGestureRecognizer) {
        guard let item = notification else { return }
        if let url = item.imageUrl {
            didSelectImage?(url)
        }
    }

    @objc private func onVideoTapped(gestureRecognizer: UITapGestureRecognizer) {
        guard let item = notification else { return }
        if let url = item.videoUrl {
            didSelectVideo?(url)
        }
    }

    override func prepareForReuse() {
        photoImage.image = nil
        videoImage.image = nil
        authorLabel.text = nil
        usernameLabel.text = nil
        super.prepareForReuse()
    }

    private func adjustConstraints(item: Notification) {
        if item.imageUrl != nil && item.videoUrl != nil {
            hashtagLabel.snp.remakeConstraints { make in
                make.trailing.equalToSuperview().inset(12)
                make.leading.equalTo(profileImage.snp.trailing).offset(10)
                make.top.equalTo(bodyLabel.snp.bottom).offset(5)
            }

            photoImage.snp.remakeConstraints { make in
                make.top.equalTo(hashtagLabel.snp.bottom).offset(10)
                make.leading.equalTo(profileImage.snp.trailing).offset(10)
                make.trailing.equalToSuperview().inset(12)
                make.height.equalTo(140)
            }

            videoImage.snp.remakeConstraints { make in
                make.top.equalTo(photoImage.snp.bottom).offset(10)
                make.leading.equalTo(profileImage.snp.trailing).offset(10)
                make.trailing.equalToSuperview().inset(12)
                make.height.equalTo(140)
                make.bottom.lessThanOrEqualTo(sourceLabel.snp.top).offset(-5)
            }
        } else if item.imageUrl != nil {
            videoImage.snp.removeConstraints()

            hashtagLabel.snp.remakeConstraints { make in
                make.trailing.equalToSuperview().inset(12)
                make.leading.equalTo(profileImage.snp.trailing).offset(10)
                make.top.equalTo(bodyLabel.snp.bottom).offset(5)
            }

            photoImage.snp.remakeConstraints { make in
                make.top.equalTo(hashtagLabel.snp.bottom).offset(10)
                make.leading.equalTo(profileImage.snp.trailing).offset(10)
                make.trailing.equalToSuperview().inset(12)
                make.height.equalTo(140)
                make.bottom.lessThanOrEqualTo(sourceLabel.snp.top).offset(-5)
            }
        } else if item.videoUrl != nil {
            photoImage.snp.removeConstraints()

            hashtagLabel.snp.remakeConstraints { make in
                make.trailing.equalToSuperview().inset(12)
                make.leading.equalTo(profileImage.snp.trailing).offset(10)
                make.top.equalTo(bodyLabel.snp.bottom).offset(5)
            }

            videoImage.snp.remakeConstraints { make in
                make.top.equalTo(hashtagLabel.snp.bottom).offset(10)
                make.leading.equalTo(profileImage.snp.trailing).offset(10)
                make.trailing.equalToSuperview().inset(12)
                make.height.equalTo(140)
                make.bottom.lessThanOrEqualTo(sourceLabel.snp.top).offset(-5)
            }
        } else {
            photoImage.snp.removeConstraints()
            videoImage.snp.removeConstraints()

            hashtagLabel.snp.remakeConstraints { make in
                make.trailing.equalToSuperview().inset(12)
                make.leading.equalTo(profileImage.snp.trailing).offset(10)
                make.top.equalTo(bodyLabel.snp.bottom).offset(5)
                make.bottom.lessThanOrEqualTo(sourceLabel.snp.top).offset(-5)
            }
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let profile = UIImageView()
        profile.layer.cornerRadius = 32
        profile.layer.borderWidth = 2.0
        profile.layer.borderColor = UIColor(named: .appTint).cgColor
        profile.clipsToBounds = true
        profile.contentMode = .scaleAspectFill
        contentView.addSubview(profile)
        profile.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().inset(12)
            make.width.height.equalTo(64)
        }
        self.profileImage = profile

        let sourceLabel = UILabel()
        sourceLabel.font = UIFont.systemFont(ofSize: 10)
        sourceLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.6)
        contentView.addSubview(sourceLabel)
        sourceLabel.snp.makeConstraints { make in
            make.leading.equalTo(profile.snp.trailing).offset(12)
            make.trailing.bottom.equalToSuperview().inset(12)
        }
        self.sourceLabel = sourceLabel

        let author = UILabel()
        author.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        author.textColor = UIColor(named: .textTitle)
        contentView.addSubview(author)
        author.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(12)
            make.leading.equalTo(profile.snp.trailing).offset(10)
            make.width.lessThanOrEqualToSuperview().priority(.required)
        }
        self.authorLabel = author

        let username = UILabel()
        username.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        username.textColor = UIColor(named: .textTitle).withAlphaComponent(0.8)
        username.adjustsFontSizeToFitWidth = true
        contentView.addSubview(username)
        username.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(12)
            make.leading.equalTo(author.snp.trailing).offset(5)
            make.trailing.lessThanOrEqualToSuperview().inset(12)
            make.bottom.equalTo(author)
        }
        self.usernameLabel = username


        let body = UILabel()
        body.font = UIFont.systemFont(ofSize: 16)
        body.textColor = UIColor(named: .textTitle)
        body.numberOfLines = 0
        body.lineBreakMode = .byWordWrapping
        contentView.addSubview(body)
        body.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(12)
            make.leading.equalTo(profile.snp.trailing).offset(10)
            make.top.equalTo(author.snp.bottom).offset(5)
        }
        self.bodyLabel = body

        let hashtag = UILabel()
        hashtag.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        hashtag.textColor = UIColor(named: .textTitle).withAlphaComponent(0.8)
        hashtag.numberOfLines = 0
        hashtag.lineBreakMode = .byWordWrapping
        contentView.addSubview(hashtag)
        hashtag.snp.makeConstraints { make in
            make.top.equalTo(body.snp.bottom)
            make.trailing.equalToSuperview().inset(12)
            make.leading.equalTo(profile.snp.trailing).offset(10)
            make.bottom.lessThanOrEqualTo(sourceLabel.snp.top)
        }
        self.hashtagLabel = hashtag

        let photo = UIImageView()
        photo.layer.cornerRadius = 6.0
        photo.clipsToBounds = true
        photo.contentMode = .scaleAspectFill
        photo.isUserInteractionEnabled = true
        photo.backgroundColor = UIColor(named: .textTitle).withAlphaComponent(0.6)

        let photoTapGesture = UITapGestureRecognizer(target: self, action: #selector(onImageTapped(gestureRecognizer:)))
        photo.addGestureRecognizer(photoTapGesture)

        contentView.addSubview(photo)
        self.photoImage = photo

        let video = UIImageView()
        video.layer.cornerRadius = 6.0
        video.clipsToBounds = true
        video.contentMode = .scaleAspectFill
        video.isUserInteractionEnabled = true
        video.backgroundColor = UIColor(named: .textTitle).withAlphaComponent(0.6)

        let playbackIcon = UIImageView()
        playbackIcon.image = UIImage(asset: Asset.icPlayback2)
        video.addSubview(playbackIcon)
        playbackIcon.snp.makeConstraints { make in
            make.width.height.equalTo(44)
            make.center.equalToSuperview()
        }

        let videoTapGesture = UITapGestureRecognizer(target: self, action: #selector(onVideoTapped(gestureRecognizer:)))
        video.addGestureRecognizer(videoTapGesture)

        contentView.addSubview(video)
        self.videoImage = video
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}