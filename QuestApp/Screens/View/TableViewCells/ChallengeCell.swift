//
// Created by Vlad Gorbunov on 16/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import AlamofireImage

class ChallengeCell: UITableViewCell {
    enum Identifiers {
        static let withImage = "challenge+image"
        static let withoutImage = "challenge-image"
    }

    private weak var thumbnailImage: UIImageView!
    private weak var titleLabel: UILabel!
    private weak var descriptionLabel: UILabel!
    private weak var hashtagLabel: UILabel!
    private weak var dueLabel: UILabel!
    private weak var photoIcon: UIImageView!
    private weak var videoIcon: UIImageView!
    private weak var pointsLabel: UILabel!

    var challenge: Challenge? {
        didSet {
            guard let challenge = challenge else { return }

            self.adjustConstraints(challenge: challenge)

            if let thumbnailUrl = challenge.imageUrl {
                thumbnailImage.af_setImage(withURL: thumbnailUrl)
            }

            titleLabel.text = challenge.name
            descriptionLabel.text = challenge.description
            hashtagLabel.text = "\(challenge.hashtagPrefix)\(challenge.hashtagSuffix)"
            dueLabel.text = L10n.Date.due(DateFormatters.sharedDayMonth.string(from: challenge.endDate))
            pointsLabel.text = L10n.General.points(challenge.defaultPoints)

            photoIcon.isHidden = !challenge.requiresPhoto
            videoIcon.isHidden = !challenge.requiresVideo
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        let image = UIImageView()
        image.layer.cornerRadius = 6.0
        image.clipsToBounds = true
        image.contentMode = .scaleToFill
        image.backgroundColor = UIColor(named: .disabledControl)
        contentView.addSubview(image)
        image.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(12)
            make.leading.trailing.equalToSuperview().inset(12)
            make.height.equalTo(180).priority(.high)
        }
        self.thumbnailImage = image

        let title = UILabel()
        title.font = UIFont.systemFont(ofSize: 18.0, weight: .bold)
        title.textColor = UIColor(named: .textTitle)
        title.numberOfLines = 2
        contentView.addSubview(title)
        title.snp.makeConstraints { make in
            make.top.equalTo(image.snp.bottom).offset(10)
            make.trailing.leading.equalToSuperview().inset(12)
        }
        self.titleLabel = title

        let description = UILabel()
        description.font = UIFont.systemFont(ofSize: 14.0)
        description.textColor = UIColor(named: .textTitle).withAlphaComponent(0.7)
        description.numberOfLines = 3
        contentView.addSubview(description)
        description.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom).offset(10)
            make.leading.trailing.equalToSuperview().inset(12)
        }
        self.descriptionLabel = description


        let bottomInfoStack = UIStackView()
        bottomInfoStack.axis = .horizontal
        bottomInfoStack.spacing = 10
        bottomInfoStack.distribution = .fill
        contentView.addSubview(bottomInfoStack)
        bottomInfoStack.snp.makeConstraints { make in
            make.top.equalTo(description.snp.bottom).offset(10)
            make.trailing.leading.bottom.equalToSuperview().inset(12)
        }

        let hashtagIcon = UIImageView()
        hashtagIcon.image = UIImage(asset: Asset.icHashtag)
        bottomInfoStack.addArrangedSubview(hashtagIcon)
        hashtagIcon.snp.makeConstraints { make in
            make.width.height.equalTo(32)
        }

        let hashtagStack = UIStackView()
        hashtagStack.axis = .vertical
        hashtagStack.spacing = 1

        bottomInfoStack.addArrangedSubview(hashtagStack)

        let hashtag = UILabel()
        hashtag.font = UIFont.systemFont(ofSize: 12.0)
        hashtag.textColor = UIColor(named: .textTitle).withAlphaComponent(0.8)
        hashtagStack.addArrangedSubview(hashtag)
        self.hashtagLabel = hashtag

        let due = UILabel()
        due.font = UIFont.systemFont(ofSize: 10.0)
        due.textColor = UIColor(named: .textTitle).withAlphaComponent(0.6)
        hashtagStack.addArrangedSubview(due)
        self.dueLabel = due

        let photo = UIImageView()
        photo.image = UIImage(asset: Asset.icPhoto1)
        bottomInfoStack.addArrangedSubview(photo)
        photo.snp.makeConstraints { make in
            make.width.height.equalTo(32)
        }
        self.photoIcon = photo

        let video = UIImageView()
        video.image = UIImage(asset: Asset.icVideo1)
        bottomInfoStack.addArrangedSubview(video)
        video.snp.makeConstraints { make in
            make.width.height.equalTo(32)
        }
        self.videoIcon = video

        let points = UILabel()
        points.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
        points.textColor = UIColor(named: .textTitle).withAlphaComponent(0.8)
        points.textAlignment = .right
        bottomInfoStack.addArrangedSubview(points)
        points.snp.makeConstraints { make in
            make.trailing.equalTo(bottomInfoStack.snp.trailing)
        }
        self.pointsLabel = points
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func adjustConstraints(challenge: Challenge) {
        if challenge.imageUrl != nil {
            thumbnailImage.snp.remakeConstraints { make in
                make.top.equalToSuperview().inset(12)
                make.leading.trailing.equalToSuperview().inset(12)
                make.height.equalTo(180)
            }
            titleLabel.snp.remakeConstraints { make in
                make.top.equalTo(thumbnailImage.snp.bottom).offset(10)
                make.leading.trailing.equalToSuperview().inset(12)
            }
        } else {
            thumbnailImage.snp.removeConstraints()

            titleLabel.snp.remakeConstraints { make in
                make.top.equalToSuperview().inset(12)
                make.leading.trailing.equalToSuperview().inset(12)
            }
        }
    }

    override func prepareForReuse() {
        thumbnailImage.image = nil
        super.prepareForReuse()
    }
}