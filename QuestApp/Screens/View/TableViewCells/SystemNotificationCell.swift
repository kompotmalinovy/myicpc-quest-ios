//
// Created by Vlad Gorbunov on 19/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class SystemNotificationCell: UITableViewCell {

    enum Identifiers {
        static let systemNotification = "system+notif"
    }

    private weak var notificationIcon: UIImageView!
    private weak var titleLabel: UILabel!
    private weak var bodyLabel: UILabel!
    private weak var hashtagLabel: UILabel!
    private weak var photoImage: UIImageView!
    private weak var videoImage: UIImageView!

    private weak var sourceLabel: UILabel!

    var notification: Notification? {
        didSet {
            guard let notification = notification else { return }

            guard notification.notificationType != Constants.NOTIFICATION_QUEST_APP && notification.notificationType != Constants.NOTIFICATION_TWITTER else { return }

            notificationIcon.image = Asset.icNotif.image
            titleLabel.text = notification.title
            bodyLabel.text = notification.body
            hashtagLabel.text = notification.hashtags?.map { L10n.Notification.hashtag($0) }.joined(separator: " ")

            sourceLabel.text = L10n.Notification.source(DateFormatters.sharedDayMonthTime.string(from: notification.submitDate), "System")
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = UIColor(named: .systemBkg).withAlphaComponent(0.5)

        let icon = UIImageView()
        icon.layer.cornerRadius = 32
        icon.clipsToBounds = true
        icon.contentMode = .scaleAspectFill
        contentView.addSubview(icon)
        icon.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().inset(12)
            make.width.height.equalTo(64)
        }
        self.notificationIcon = icon

        let sourceLabel = UILabel()
        sourceLabel.font = UIFont.systemFont(ofSize: 10)
        sourceLabel.textColor = UIColor(named: .textTitle).withAlphaComponent(0.6)
        contentView.addSubview(sourceLabel)
        sourceLabel.snp.makeConstraints { make in
            make.leading.equalTo(icon.snp.trailing).offset(12)
            make.trailing.bottom.equalToSuperview().inset(12)
        }
        self.sourceLabel = sourceLabel

        let title = UILabel()
        title.font = UIFont.systemFont(ofSize: 18.0, weight: .bold)
        title.textColor = UIColor(named: .textTitle)
        title.numberOfLines = 2
        contentView.addSubview(title)
        title.snp.makeConstraints { make in
            make.top.trailing.equalToSuperview().inset(12)
            make.leading.equalTo(icon.snp.trailing).offset(10)
        }
        self.titleLabel = title

        let body = UILabel()
        body.font = UIFont.systemFont(ofSize: 14.0)
        body.textColor = UIColor(named: .textTitle).withAlphaComponent(0.8)
        body.numberOfLines = 0
        body.lineBreakMode = .byWordWrapping
        contentView.addSubview(body)
        body.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(12)
            make.leading.equalTo(icon.snp.trailing).offset(10)
            make.top.equalTo(title.snp.bottom).offset(5)
        }
        self.bodyLabel = body

        let hashtag = UILabel()
        hashtag.font = UIFont.systemFont(ofSize: 14.0, weight: .medium)
        hashtag.textColor = UIColor(named: .textTitle).withAlphaComponent(0.9)
        hashtag.numberOfLines = 0
        hashtag.lineBreakMode = .byWordWrapping
        contentView.addSubview(hashtag)
        hashtag.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(12)
            make.leading.equalTo(icon.snp.trailing).offset(10)
            make.top.equalTo(body.snp.bottom).offset(5)
            make.bottom.lessThanOrEqualTo(sourceLabel.snp.top)
        }
        self.hashtagLabel = hashtag
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}