//
// Created by Vlad Gorbunov on 07/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import UIKit
import SnapKit
import Charts

class LeaderboardRowCell: UITableViewCell, ChartViewDelegate {
    enum Identifiers {
        static let withGraph = "row+name+graph"
    }

    private weak var titleLabel: UILabel!
    private weak var pointsLabel: UILabel!
    private weak var chartBar: HorizontalBarChartView!

    var leaderboardRow: LeaderboardRow? {
        didSet {
            guard let row = leaderboardRow else { return }
            titleLabel.text = row.name
            pointsLabel.text = L10n.Leaderboard.points(row.points)

            let entry = BarChartDataEntry(x: 0, y: Double(row.points))
            let dataset = BarChartDataSet(values: [entry], label: nil)
            dataset.colors = [UIColor(named: .successGreen)]

            let data = BarChartData(dataSet: dataset)
            data.setDrawValues(false)
            chartBar.data = data
            chartBar.leftAxis.axisMaximum = Double(row.maxPoints)
            chartBar.rightAxis.axisMaximum = Double(row.maxPoints)
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        isUserInteractionEnabled = false
        let contentStack = UIStackView()
        contentStack.axis = .vertical
        contentStack.spacing = 5
        contentStack.distribution = .fill
        contentView.addSubview(contentStack)
        contentStack.snp.makeConstraints { make in
            make.edges.equalTo(0).inset(10)
        }

        let infoStack = UIStackView()
        infoStack.axis = .horizontal
        infoStack.spacing = 10
        infoStack.distribution = .equalSpacing
        contentStack.addArrangedSubview(infoStack)

        let title = UILabel()
        title.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        title.textColor = UIColor(named: .textTitle)
        infoStack.addArrangedSubview(title)
        self.titleLabel = title

        let points = UILabel()
        points.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        points.textColor = UIColor(named: .textTitle).withAlphaComponent(0.8)
        points.textAlignment = .right
        infoStack.addArrangedSubview(points)
        self.pointsLabel = points


        let bar = HorizontalBarChartView()
        bar.noDataText = "No data"
        bar.chartDescription = nil
        bar.legend.enabled = false
        bar.drawBarShadowEnabled = true
        bar.drawGridBackgroundEnabled = false
        bar.drawValueAboveBarEnabled = true
        bar.isUserInteractionEnabled = false
        bar.minOffset = 0

        let xAxis = bar.xAxis
        xAxis.drawAxisLineEnabled = false
        xAxis.drawLabelsEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.granularity = 1
        xAxis.enabled = false


        bar.leftAxis.drawAxisLineEnabled = false
        bar.leftAxis.drawLabelsEnabled = false
        bar.leftAxis.drawGridLinesEnabled = false
        bar.leftAxis.axisMinimum = 0

        bar.rightAxis.drawLabelsEnabled = false
        bar.rightAxis.drawAxisLineEnabled = false
        bar.rightAxis.drawGridLinesEnabled = false
        bar.rightAxis.axisMinimum = 0

        contentStack.addArrangedSubview(bar)
        bar.snp.makeConstraints { make in
            make.height.equalTo(40)
        }
        self.chartBar = bar
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        chartBar.data = nil
    }
}