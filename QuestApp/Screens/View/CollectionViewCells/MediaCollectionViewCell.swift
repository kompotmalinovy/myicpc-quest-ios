//
// Created by Vlad Gorbunov on 21/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Gallery
import Photos

class MediaCollectionViewCell: UICollectionViewCell {

    enum Identifiers {
        static let image = "media+image"
        static let video = "media+video"
    }

    private weak var imageOverlay: UIImageView!
    private weak var deleteButton: UIButton!
    private weak var imageFileIcon: UIImageView!
    private weak var videoFileIcon: UIImageView!

    var media: MediaType? {
        didSet {
            guard let media = media else { return }

            if case .photo(let image) = media {
                DispatchQueue.global().async {
                    image.resolve { img in
                        DispatchQueue.main.async {
                            self.imageFileIcon.isHidden = false
                            self.imageOverlay.image = img
                        }
                    }
                }
            }

            if case .video(let video) = media {
                DispatchQueue.global().async {
                    video.fetchThumbnail(size: PHImageManagerMaximumSize) { thumb in
                        DispatchQueue.main.async {
                            self.videoFileIcon.isHidden = false
                            self.imageOverlay.image = thumb
                        }
                    }
                }
            }
        }
    }

    var remove: (() -> Void)?

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = .clear

        let imageOverlay = UIImageView()
        imageOverlay.layer.cornerRadius = 6
        imageOverlay.clipsToBounds = true
        imageOverlay.contentMode = .scaleAspectFill
        imageOverlay.isUserInteractionEnabled = true
        contentView.addSubview(imageOverlay)
        imageOverlay.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.imageOverlay = imageOverlay

        let cancel = UIButton()
        cancel.setImage(Asset.icCancelRounded.image, for: .normal)
        cancel.addTarget(self, action: #selector(didSelectRemove), for: .touchUpInside)
        imageOverlay.addSubview(cancel)
        cancel.snp.makeConstraints { make in
            make.top.trailing.equalToSuperview().inset(2)
            make.height.width.equalToSuperview().dividedBy(12)
        }
        self.deleteButton = cancel

        let imageFile = UIImageView(image: Asset.icImageFile.image)
        imageOverlay.addSubview(imageFile)
        imageFile.isHidden = true
        imageFile.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().inset(2)
            make.height.width.equalToSuperview().dividedBy(12)
        }
        self.imageFileIcon = imageFile

        let videoFile = UIImageView(image: Asset.icVideoFile.image)
        videoFile.tintColor = UIColor(named: .textTitle)
        imageOverlay.addSubview(videoFile)
        videoFile.isHidden = true
        videoFile.snp.makeConstraints { make in
            make.top.leading.equalToSuperview().inset(2)
            make.height.width.equalToSuperview().dividedBy(12)
        }
        self.videoFileIcon = videoFile
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func didSelectRemove() {
        remove?()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageFileIcon.isHidden = true
        videoFileIcon.isHidden = true
        imageOverlay.image = nil
    }
}
