//
// Created by Vlad Gorbunov on 25/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import RxSwift
import RxBluetoothKit

struct BTDevice {
    let name: String?
    let macAddress: String? = nil
}

protocol RxBLEDeviceProviding {
    func observeBLEDevices(timeInterval: RxTimeInterval) -> Observable<[BTDevice]>
}

class RxBLEDeviceProvider: RxBLEDeviceProviding {
    private let centralManager = CentralManager(queue: .global())

    func observeBLEDevices(timeInterval: RxTimeInterval) -> Observable<[BTDevice]> {
        return centralManager.observeState()
                .startWith(centralManager.state)
                .filter { $0 == .poweredOn }
                .timeout(timeInterval, scheduler: ConcurrentDispatchQueueScheduler(qos: .utility))
                .take(1)
                .flatMap { _ in
                    self.centralManager.scanForPeripherals(withServices: nil)
                            .map { BTDevice(name: $0.peripheral.name) }
                            .take(timeInterval, scheduler: ConcurrentDispatchQueueScheduler(qos: .utility))
                            .toArray()
                }
                .catchErrorJustReturn([BTDevice]())
    }
}
