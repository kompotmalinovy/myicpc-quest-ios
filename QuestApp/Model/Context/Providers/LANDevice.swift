//
// Created by Vlad Gorbunov on 25/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import RxSwift

struct LANDevice {
    let ipAddress: String
    let macAddress: String
}

protocol RxLANDeviceProviding {
    func observeLANDevices(timeInterval: RxTimeInterval) -> Observable<[LANDevice]>
}

class RxLANDeviceProvider: RxLANDeviceProviding {
    private let lanScanner: RxLanScanning = RxLanScanner()

    func observeLANDevices(timeInterval: RxTimeInterval) -> Observable<[LANDevice]> {
        return lanScanner.observeLANDevices()
                .map { LANDevice(ipAddress: $0.ipAddress, macAddress: $0.macAddress) }
                .take(timeInterval, scheduler: ConcurrentDispatchQueueScheduler(qos: .utility))
                .toArray()
                .catchErrorJustReturn([LANDevice]())
    }
}
