//
// Created by Vlad Gorbunov on 19/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import RxSwift
import SystemConfiguration.CaptiveNetwork

struct ConnectedWifi {
    let ssid: String?
    let bssid: String?
    let state: String? = nil
    let rssi: Int? = nil
    let linkSpeed: Int? = nil
    let frequency: Int? = nil
    let ipAddress: String? = nil
}

protocol RxConnectedNetworkProviding {
    func observeConnectedNetwork() -> Observable<ConnectedWifi?>
}

class RxConnectedNetworkProvider: RxConnectedNetworkProviding {
    func observeConnectedNetwork() -> Observable<ConnectedWifi?> {
        var ssid: String? = nil
        var bssid: String? = nil

        if let interfaces = CNCopySupportedInterfaces() as? NSArray {
            for interface in interfaces {
                if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as? NSDictionary {
                    ssid = interfaceInfo[kCNNetworkInfoKeySSID] as? String
                    bssid = interfaceInfo[kCNNetworkInfoKeyBSSID] as? String
                }
            }
        }
        return Observable.just(ssid != nil && bssid != nil ? ConnectedWifi(ssid: ssid, bssid: bssid) : nil)
    }
}