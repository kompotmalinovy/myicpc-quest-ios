//
// Created by Vlad Gorbunov on 19/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import CoreLocation
import RxCoreLocation
import RxSwift

struct Location {
    let timestamp: Date
    let latitude: Double
    let longitude: Double
}

extension CLLocation {
    func toLocation() -> Location {
        return Location(timestamp: timestamp, latitude: Double(coordinate.latitude), longitude: Double(coordinate.longitude))
    }
}

protocol RxLocationProviding {
    func observeLocation() -> Observable<Location?>
}

class RxLocationProvider: RxLocationProviding {
    private let locationManager = CLLocationManager()

    func observeLocation() -> Observable<Location?> {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        return locationManager.rx.location
            .take(1)
            .map { $0?.toLocation() }
            .do(onDispose: { self.locationManager.stopUpdatingLocation() })
    }
}
