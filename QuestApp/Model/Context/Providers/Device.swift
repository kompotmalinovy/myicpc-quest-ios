//
// Created by Vlad Gorbunov on 19/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

struct Device {
    let operatingSystem: String
    let version: String
    let deviceBrand: String
    let deviceModel: String
    let serial: String?
}

extension UIDevice {
    func toDevice() -> Device {
        return Device(
                operatingSystem: systemName,
                version: systemVersion,
                deviceBrand: "Apple",
                deviceModel: name,
                serial: identifierForVendor?.uuidString
        )
    }
}

protocol RxDeviceProviding {
    func observeDevice() -> Observable<Device>
}

class RxDeviceProvider: RxDeviceProviding {
    func observeDevice() -> Observable<Device> {
        return Observable.just(UIDevice.current.toDevice())
    }
}