//
// Created by Vlad Gorbunov on 22/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

struct Context {
    let source: String
    let timestamp: Date
    let device: Device
    let location: Location?
    let connectedNetwork: ConnectedWifi?
    let bleDevices: [BTDevice]
    let lanDevices: [LANDevice]
}