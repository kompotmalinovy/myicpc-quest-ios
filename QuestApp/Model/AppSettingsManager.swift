//
// Created by Vlad Gorbunov on 15/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

/*
    Some stuff common to specific app installation

    - launchedBefore: Bool - indicates if this is first app launch
            since auth stuff we put in keychain will not be deleted across app reinstall
            we need some indicator that this is actually first time app is being started
    - user: User? - user that comes from api, this is basically a preparation for
            a twitter-like user button in navigation bar, making user request every time to show
            an image (which is also loaded by the way) would not make sense
    - contextPermissionRequested: Bool - indicates whether context send permission was requested
    - contextPermissionGranted: Bool - indicates if context is allowed by user to be gathered and sent
*/
protocol AppSettingsManaging: class {
    var launchedBefore: Bool { get set }
    var contextPermissionRequested: Bool { get set }
    var contextPermissionGranted: Bool { get set }
    var user: User? { get set }
}

extension UserDefaults: AppSettingsManaging {
    private enum Keys {
        static let isFirstLaunch = "first+launch"
        static let contextDialogWasDisplayed = "context+dialog"
        static let contextPermissionGranted = "context+granted"
        static let user = "quest+user"
    }

    var launchedBefore: Bool {
        get {
            return bool(forKey: Keys.isFirstLaunch)
        }
        set {
            set(newValue, forKey: Keys.isFirstLaunch)
            synchronize()
        }
    }

    var contextPermissionRequested: Bool {
        get {
            return bool(forKey: Keys.contextDialogWasDisplayed)
        }
        set {
            set(newValue, forKey: Keys.contextDialogWasDisplayed)
            synchronize()
        }
    }

    var contextPermissionGranted: Bool {
        get {
            return bool(forKey: Keys.contextPermissionGranted)
        }
        set {
            set(newValue, forKey: Keys.contextPermissionGranted)
            synchronize()
        }
    }

    var user: User? {
        get {
            if let storedUser = UserDefaults.standard.object(forKey: Keys.user),
               let userData = storedUser as? Data, 
               let unarchivedData = NSKeyedUnarchiver.unarchiveObject(with: userData) as? Data {
                do {
                    let jsonDecoder = JSONDecoder()
                    let user = try jsonDecoder.decode(User.self, from: unarchivedData)
                    return user
                } catch {
                    print("user could not be decoded")
                    return nil
                }
            }
            return nil
        }

        set {
            if let user = newValue {
                do {
                    let encoder = JSONEncoder()
                    let data = try encoder.encode(user)
                    let jsonArchived = NSKeyedArchiver.archivedData(withRootObject: data)

                    set(jsonArchived, forKey: Keys.user)
                    synchronize()
                } catch {
                    print("setting user failed, user was not encoded")
                }
            } else {
                removeObject(forKey: Keys.user)
                synchronize()
            }
        }
    }
}