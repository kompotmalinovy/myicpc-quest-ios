//
// Created by Vlad Gorbunov on 18/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

class Constants {
    static let NOTIFICATION_QUEST_APP = "Mobile App"
    static let NOTIFICATION_TWITTER = "Tweet"
    static let IMAGE_SEND_LIMIT = 1
    static let VIDEO_SEND_LIMIT = 1
}