//
// Created by Vlad Gorbunov on 21/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

enum State<T> {
    case idle
    case empty
    case error(Error)
    case loading
    case loaded(T)
    case reloading
}

extension State: Equatable {
    static func ==(lhs: State<T>, rhs: State<T>) -> Bool {
        switch (lhs, rhs) {
            case (.loaded, .loaded):
                return true
            case (.reloading, .reloading):
                return true
            case (.error, .error):
                return true
            case (.loading, .loading):
                return true
            case (.empty, .empty):
                return true
            default:
                return false
        }
    }
}