//
// Created by Vlad Gorbunov on 15/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire

class ApiDescription {

    func getFeed(token: String) -> Observable<[Notification]> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "whats-happening-now", headers: ["Authorization": "Bearer \(token)"])
                .map(to: [Notification].self)
    }

    func getTimeline(contestCode: String, token: String) -> Observable<[Notification]> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "timeline/\(contestCode)", headers: ["Authorization": "Bearer \(token)"])
                .map(to: [Notification].self)
    }

    func getChallenges(token: String) -> Observable<[Challenge]> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "challenges", headers: ["Authorization": "Bearer \(token)"])
                .map(to: [Challenge].self)
    }

    func getChallenge(hashtagSuffix: String, token: String) -> Observable<Challenge> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "challenges/\(hashtagSuffix)", headers: ["Authorization": "Bearer \(token)"])
                .map(to: Challenge.self)
    }

    func getAcceptedSubmissions(hashtagSuffix: String, token: String) -> Observable<[NotificationResponse]> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "challenges\(hashtagSuffix)/accepted-submission", headers: ["Authorization": "Bearer \(token)"])
                .map(to: [NotificationResponse].self)
    }

    func getPendingSubmissions(hashtagSuffix: String, token: String) -> Observable<[NotificationResponse]> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "challenges\(hashtagSuffix)/pending-submission", headers: ["Authorization": "Bearer \(token)"])
                .map(to: [NotificationResponse].self)
    }

    func getRejectedSubmissions(hashtagSuffix: String, token: String) -> Observable<[NotificationResponse]> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "challenges\(hashtagSuffix)/rejected-submission", headers: ["Authorization": "Bearer \(token)"])
                .map(to: [NotificationResponse].self)
    }

    func getLeaderboardTypes(token: String) -> Observable<[Leaderboard]> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "leaderboards", headers: ["Authorization": "Bearer \(token)"])
                .map(to: [Leaderboard].self)
    }

    func getLeaderboard(urlCode: String, token: String) -> Observable<[LeaderboardRowResponse]> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "leaderboards/\(urlCode)", headers: ["Authorization": "Bearer \(token)"])
                .map(to: [LeaderboardRowResponse].self)
    }

    func getTeamLeaderboard(token: String) -> Observable<[LeaderboardRowResponse]> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "team-leaderboard", headers: ["Authorization": "Bearer \(token)"])
                .map(to: [LeaderboardRowResponse].self)
    }

    func getUser(contestCode: String, token: String) -> Observable<User> {
        return RxAlamofire.requestData(.get, Environment.API.baseURL + "user/\(contestCode)", headers: ["Authorization": "Bearer \(token)"])
                .map(to: User.self)
    }

    func postTimelineSubmission(contestCode: String, submission: Submission, token: String) -> Observable<Void> {
        return RxAlamofire.request(
                    .post,
                    Environment.API.baseURL + "timeline/\(contestCode)",
                    parameters: ["hashtag": submission.hashtag,
                                 "message": submission.message,
                                 "image": submission.image,
                                 "video": submission.video,
                                 "context": submission.context],
                    encoding: JSONEncoding.default,
                    headers: ["Authoriation": "Bearer \(token)"]
            )
                .map { _ in }
    }
}

extension ObservableType {
    func map<T: Codable>(to: T.Type) -> Observable<T> {
        return map { data -> T in
            let response = data as? (HTTPURLResponse, Data)
            if let code = response?.0.statusCode, code == 401 {
                throw AuthError.unauthorized
            } else if let data = response?.1 {
                let decoder = JSONDecoder()
                return try decoder.decode(T.self, from: data)
            } else {
                throw DataError.dataDeserializationError
            }
        }
    }
}

enum DataError: Error {
    case dataDeserializationError
}
