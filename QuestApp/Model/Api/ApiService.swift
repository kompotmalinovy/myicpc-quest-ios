//
// Created by Vlad Gorbunov on 19/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import RxAlamofire
import RxSwift


protocol ApiInteracting {
    func getFeed() -> Observable<[Notification]>
    func getTimeline(contestCode: String) -> Observable<[Notification]>
    func getChallenges() -> Observable<[Challenge]>
    func getChallenge(hashtagSuffix: String) -> Observable<Challenge>
    func getAcceptedSubmissions(hashtagSuffix: String) -> Observable<[NotificationResponse]>
    func getPendingSubmissions(hashtagSuffix: String) -> Observable<[NotificationResponse]>
    func getRejectedSubmissions(hashtagSuffix: String) -> Observable<[NotificationResponse]>
    func getLeaderboardTypes() -> Observable<[Leaderboard]>
    func getLeaderboard(urlCode: String) -> Observable<[LeaderboardRowResponse]>
    func getTeamLeaderboard() -> Observable<[LeaderboardRowResponse]>
    func getUser(contestCode: String) -> Observable<User>
    func postSubmission(submission: Submission) -> Observable<Void>
}

class ApiInteractor: ApiInteracting {
    private let decoder = JSONDecoder()
    private let userManager: UserManaging
    private let apiDescription: ApiDescription

    init(userManager: UserManaging, apiDescription: ApiDescription) {
        self.userManager = userManager
        self.apiDescription = apiDescription
    }

    func getFeed() -> Observable<[Notification]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getFeed(token: token)
                }
    }

    func getTimeline(contestCode: String) -> Observable<[Notification]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getTimeline(contestCode: contestCode, token: token)
                }
    }

    func getChallenges() -> Observable<[Challenge]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getChallenges(token: token)
                }
    }

    func getChallenge(hashtagSuffix: String) -> Observable<Challenge> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getChallenge(hashtagSuffix: hashtagSuffix, token: token)
                }
    }

    func getAcceptedSubmissions(hashtagSuffix: String) -> Observable<[NotificationResponse]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getAcceptedSubmissions(hashtagSuffix: hashtagSuffix, token: token)
                }
    }

    func getPendingSubmissions(hashtagSuffix: String) -> Observable<[NotificationResponse]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getPendingSubmissions(hashtagSuffix: hashtagSuffix, token: token)
                }
    }

    func getRejectedSubmissions(hashtagSuffix: String) -> Observable<[NotificationResponse]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getPendingSubmissions(hashtagSuffix: hashtagSuffix, token: token)
                }
    }

    func getLeaderboardTypes() -> Observable<[Leaderboard]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getLeaderboardTypes(token: token)
                }
    }

    func getLeaderboard(urlCode: String) -> Observable<[LeaderboardRowResponse]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getLeaderboard(urlCode: urlCode, token: token)
                }
    }

    func getTeamLeaderboard() -> Observable<[LeaderboardRowResponse]> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getTeamLeaderboard(token: token)
                }
    }

    func getUser(contestCode: String) -> Observable<User> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.getUser(contestCode: contestCode, token: token)
                }
    }

    func postSubmission(submission: Submission) -> Observable<Void> {
        return userManager.observeFreshToken()
                .flatMap { token in
                    self.apiDescription.postTimelineSubmission(contestCode: Environment.Contest.code, submission: submission, token: token)
                }
    }
}