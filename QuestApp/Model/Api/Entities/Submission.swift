//
// Created by Vlad Gorbunov on 19/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

struct Submission {
    let hashtag: String
    let message: String?
    let image: File?
    let video: File?
    let context: Context?
}