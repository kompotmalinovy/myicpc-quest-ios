//
// Created by Vlad Gorbunov on 24/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

struct File {
    let filename: String
    let mime: String
    let data: Data
}