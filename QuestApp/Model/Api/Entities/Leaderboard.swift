//
// Created by Vlad Gorbunov on 01/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

enum LeaderboardType {
    case generic(Leaderboard)
    case team
}

struct LeaderboardRowResponse: Codable {
    let name: String
    let points: Int

    enum CodingKeys: String, CodingKey {
        case name
        case points
    }
}

struct LeaderboardRow: Codable {
    let name: String
    let points: Int
    let maxPoints: Int
    
    enum CodingKeys: String, CodingKey {
        case name
        case points
        case maxPoints
    }
}

struct Leaderboard: Codable {
    let name: String
    let urlCode: String
    let roles: [String]

    enum CodingKeys: String, CodingKey {
        case name
        case urlCode
        case roles
    }
}