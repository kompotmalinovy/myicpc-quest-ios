//
// Created by Vlad Gorbunov on 19/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

enum SubmissionType {
    case all
    case accepted
    case pending
    case rejected
}

struct Notification: Codable {
    let title: String?
    let body: String
    let url: URL?
    let imageUrl: URL?
    let videoUrl: URL?
    let thumbnailUrl: URL?
    let authorName: String?
    let authorUsername: String?
    let profilePictureUrl: URL?
    let hashtags: [String]?
    let code: String
    let submitDate: Date
    let notificationType: String

    enum CodingKeys: String, CodingKey {
        case title
        case body
        case url
        case imageUrl
        case videoUrl
        case thumbnailUrl
        case authorName
        case authorUsername
        case profilePictureUrl
        case hashtags
        case code
        case submitDate = "timestamp"
        case notificationType
    }
}

struct NotificationResponse: Codable {
    let questPoints: Int
    let reasonToReject: String
    let notification: Notification
    let state: String

    enum CodingKeys: String, CodingKey {
        case questPoints
        case reasonToReject
        case notification
        case state
    }
}