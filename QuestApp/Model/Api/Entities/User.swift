//
// Created by Vlad Gorbunov on 09/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

struct User: Codable {
    let externalId: CLong
    let name: String
    let surname: String
    let twitterUsername: String
    let profilePictureUrl: URL?

    enum CodingKeys: String, CodingKey {
        case externalId
        case name = "firstName"
        case surname = "lastName"
        case twitterUsername
        case profilePictureUrl
    }
}