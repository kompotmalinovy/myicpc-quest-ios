//
// Created by Vlad Gorbunov on 21/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

struct Challenge: Codable {
    let name: String
    let description: String
    let requiresPhoto: Bool
    let requiresVideo:  Bool
    let defaultPoints: Int
    let imageUrl: URL?
    let hashtagSuffix: String
    let hashtagPrefix: String
    let startDate: Date
    let endDate: Date

    enum CodingKeys: String, CodingKey {
        case name
        case description
        case requiresPhoto
        case requiresVideo
        case defaultPoints
        case imageUrl = "imageURL"
        case hashtagSuffix
        case hashtagPrefix
        case startDate
        case endDate
    }
}