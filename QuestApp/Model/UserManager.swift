//
// Created by Vlad Gorbunov on 16/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import AppAuth
import RxSwift

protocol AuthStateManaging: class {
    var isAuthorized: Bool { get }
    func updateAuthState(state: OIDAuthState)
    func logout()
    func observeAuthChanges() -> Observable<LoginState>
    func authRequest() -> OIDAuthorizationRequest
    func observeFreshToken() -> Observable<String>
}

protocol UserProvider: class {
    var user: User? { get set }
}

typealias UserManaging = AuthStateManaging & UserProvider


class UserManager: NSObject, UserManaging, OIDAuthStateChangeDelegate {
    // This one basically acts like a signal producer for coordinator to switch views
    private let authSubject: BehaviorSubject<LoginState> = BehaviorSubject(value: .empty)
    private let keychainManager: KeychainManaging
    private let settingsManager: AppSettingsManaging

    init(keychainManager: KeychainManaging, settingsManager: AppSettingsManaging) {
        self.keychainManager = keychainManager
        self.settingsManager = settingsManager

        self.authState = {
            if !settingsManager.launchedBefore {
                try? keychainManager.delete()
                settingsManager.launchedBefore = true
                return nil
            } else {
                return keychainManager.load()
            }
        }()
    }

    private var authState: OIDAuthState? {
        didSet {
            authState?.stateChangeDelegate = self
        }
    }

    var user: User? {
        get { return settingsManager.user }
        set { settingsManager.user = newValue }
    }

    var isAuthorized: Bool {
        if let state = authState {
            return state.isAuthorized && settingsManager.launchedBefore
        }
        return false
    }

    func didChange(_ state: OIDAuthState) {
        updateAuthState(state: state)
    }

    func updateAuthState(state: OIDAuthState) {
        if let authState = authState {
            authState.update(with: state.lastAuthorizationResponse, error: state.authorizationError)
        } else {
            self.authState = state
        }
        try? keychainManager.save(authState: state)
    }

    func authRequest() -> OIDAuthorizationRequest {
        let authEndpoint = URL(string: Environment.Auth.authEndpoint)
        let tokenEndpoint = URL(string: Environment.Auth.tokenEndpoint)
        let redirect = URL(string: Environment.Auth.redirectURL)

        let config = OIDServiceConfiguration(authorizationEndpoint: authEndpoint!, tokenEndpoint: tokenEndpoint!)
        let request = OIDAuthorizationRequest(configuration: config,
                clientId: Environment.Auth.clientId,
                scopes: [OIDScopeOpenID],
                redirectURL: redirect!,
                responseType: OIDResponseTypeCode,
                additionalParameters: nil)

        return request
    }

    // MARK: consider using Single
    func observeFreshToken() -> Observable<String> {
        return Observable<(String)>.create { observer in

            guard let state = self.authState else {
                observer.onError(AuthError.authStateMissing)
                return Disposables.create()
            }

            state.performAction { (accessToken, idToken, error) in
                if let error = error {
                    self.logout()
                    observer.onError(error)
                } else {
                    guard let access = accessToken else {
                        observer.onError(AuthError.tokenNotPresent)
                        return
                    }
                    observer.onNext(access)
                }
            }
            return Disposables.create()
        }
    }

    func logout() {
        try? keychainManager.delete()
        settingsManager.user = nil
        authState = nil
        authSubject.onNext(.loggedOut)
    }

    func observeAuthChanges() -> Observable<LoginState> {
        return authSubject
    }
}

enum LoginState {
    case empty
    case loggedOut
}

enum AuthError: Error {
    case tokenNotPresent
    case authStateMissing
    case unauthorized
}
