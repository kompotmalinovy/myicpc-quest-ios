//
// Created by Vlad Gorbunov on 09/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import RxSwift

protocol ChallengeRepositoring {
    func observeChallenges() -> Observable<[Challenge]>
    func observeChallenge(hashtagSuffix: String) -> Observable<Challenge>
}

class ChallengeRepository: ChallengeRepositoring {
    private let apiInteractor: ApiInteracting

    init(apiInteractor: ApiInteracting) {
        self.apiInteractor = apiInteractor
    }

    func observeChallenges() -> Observable<[Challenge]> {
        return apiInteractor.getChallenges()
    }

    func observeChallenge(hashtagSuffix: String) -> Observable<Challenge> {
        return apiInteractor.getChallenge(hashtagSuffix: hashtagSuffix)
    }
}