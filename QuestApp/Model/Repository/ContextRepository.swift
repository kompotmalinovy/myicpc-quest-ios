//
// Created by Vlad Gorbunov on 19/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation
import RxSwift

protocol ContextRepositoring: class {
    func observeContext() -> Observable<Context?>
}

class ContextRepository: ContextRepositoring {

    private enum Values {
        static let bleScanTimeout = 3.0
        static let lanScanTimeout = 3.0
    }

    private let appSettings: AppSettingsManaging
    private let deviceProvider: RxDeviceProvider
    private let locationProvider: RxLocationProvider
    private let networkProvider: RxConnectedNetworkProvider
    private let lanDeviceProvider: RxLANDeviceProvider
    private let bleDeviceProvider: RxBLEDeviceProvider

    init(appSettings: AppSettingsManaging,
         deviceProvider: RxDeviceProvider,
         locationProvider: RxLocationProvider,
         networkProvider: RxConnectedNetworkProvider,
         lanDeviceProvider: RxLANDeviceProvider,
         bleDeviceProvider: RxBLEDeviceProvider) {
        
        self.appSettings = appSettings
        self.deviceProvider = deviceProvider
        self.locationProvider = locationProvider
        self.networkProvider = networkProvider
        self.lanDeviceProvider = lanDeviceProvider
        self.bleDeviceProvider = bleDeviceProvider
    }

    func observeContext() -> Observable<Context?> {
        guard appSettings.contextPermissionGranted else {
            return Observable.just(nil)
        }
        
        return Observable.combineLatest(
                observeAppSource(),
                observeTimestamp(),
                locationProvider.observeLocation(),
                deviceProvider.observeDevice(),
                networkProvider.observeConnectedNetwork(),
                bleDeviceProvider.observeBLEDevices(timeInterval: Values.bleScanTimeout),
                lanDeviceProvider.observeLANDevices(timeInterval: Values.lanScanTimeout)
        ) { source, timestamp, location , device, connectedNetwork, bleDevices, lanDevices in
            Context(
                source: source,
                timestamp: timestamp,
                device: device,
                location: location,
                connectedNetwork: connectedNetwork,
                bleDevices: bleDevices,
                lanDevices: lanDevices
            )
        }
    }

    private func observeAppSource() -> Observable<String> {
        return Observable.just(Environment.Context.appVersion)
    }

    private func observeTimestamp() -> Observable<Date> {
        return Observable.just(Date())
    }
}