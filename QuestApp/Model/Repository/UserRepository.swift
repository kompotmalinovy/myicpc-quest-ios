//
// Created by Vlad Gorbunov on 15/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import RxSwift
import Foundation

protocol UserRepositoring: class {
    func observeUser() -> Observable<User>
}

class UserRepository: UserRepositoring {
    private let apiInteractor: ApiInteracting

    init(apiInteractor: ApiInteracting) {
        self.apiInteractor = apiInteractor
    }

    func observeUser() -> Observable<User> {
        return apiInteractor.getUser(contestCode: Environment.Contest.code)
    }
}