//
// Created by Vlad Gorbunov on 09/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import RxSwift

protocol LeaderboardRepositoring {
    func observeLeaderboards() -> Observable<[LeaderboardType]>
    func observeLeaderboard(type: LeaderboardType) -> Observable<[LeaderboardRow]>
}

class LeaderboardRepository: LeaderboardRepositoring {
    private let apiInteractor: ApiInteracting

    init(apiInteractor: ApiInteracting) {
        self.apiInteractor = apiInteractor
    }

    func observeLeaderboards() -> Observable<[LeaderboardType]> {
        return apiInteractor.getLeaderboardTypes()
                .map { $0.map { LeaderboardType.generic($0)} + [LeaderboardType.team] }
    }

    func observeLeaderboard(type: LeaderboardType) -> Observable<[LeaderboardRow]> {
        let observableSource: Observable<[LeaderboardRowResponse]> = {
            switch type {
                case .team: return apiInteractor.getTeamLeaderboard()
                case .generic(let item): return apiInteractor.getLeaderboard(urlCode: item.urlCode)
            }
        }()

        return observableSource
                .map { $0.sorted(by: { $0.points > $1.points }) }
                .map { ($0, $0.map { $0.points }.max()) }
                .map { (arg: ([LeaderboardRowResponse], Int?)) in
                    let (items, max) = arg
                    return max != nil ? items.map { LeaderboardRow(name: $0.name, points: $0.points, maxPoints: max!) } : [LeaderboardRow]()
                }
    }
}
