//
// Created by Vlad Gorbunov on 09/04/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import RxSwift
import Foundation

protocol NotificationRepositoring {
    func observeFeed() -> Observable<[Notification]>
    func observeTimeline() -> Observable<[Notification]>
    func observeAcceptedSubmissions(hashtagSuffix: String) -> Observable<[Notification]>
    func observePendingSubmissions(hashtagSuffix: String) -> Observable<[Notification]>
    func observeRejectedSubmissions(hashtagSuffix: String) -> Observable<[Notification]>
    func sendSubmission(hashtagObservable: Observable<String>, messageObservable: Observable<String>, imagesObservable: Observable<[Data]>, videosObservable: Observable<[Data]>) -> Observable<Void>
    func observeSilentReload() -> Observable<Bool>
}

class NotificationRepository: NotificationRepositoring {
    private let apiInteractor: ApiInteracting
    private let contextRepository: ContextRepositoring
    private let shouldReloadForNewSubmissions: Variable<Bool>

    init(apiInteractor: ApiInteracting, contextRepository: ContextRepositoring) {
        self.apiInteractor = apiInteractor
        self.contextRepository = contextRepository
        self.shouldReloadForNewSubmissions = Variable(false)
    }

    func observeSilentReload() -> Observable<Bool> {
        return shouldReloadForNewSubmissions.asObservable()
                .delay(1, scheduler: ConcurrentDispatchQueueScheduler(qos: .background))
                .do(onNext: { _ in self.shouldReloadForNewSubmissions.value = false })
    }

    func observeFeed() -> Observable<[Notification]> {
        return apiInteractor.getFeed()
    }

    func observeTimeline() -> Observable<[Notification]> {
        return apiInteractor.getTimeline(contestCode: Environment.Contest.code)
    }

    func observeAcceptedSubmissions(hashtagSuffix: String) -> Observable<[Notification]> {
        return apiInteractor.getAcceptedSubmissions(hashtagSuffix: hashtagSuffix)
                .map { $0.map { $0.notification } }
    }

    func observePendingSubmissions(hashtagSuffix: String) -> Observable<[Notification]> {
        return apiInteractor.getAcceptedSubmissions(hashtagSuffix: hashtagSuffix)
                .map { $0.map { $0.notification } }
    }

    func observeRejectedSubmissions(hashtagSuffix: String) -> Observable<[Notification]> {
        return apiInteractor.getAcceptedSubmissions(hashtagSuffix: hashtagSuffix)
                .map { $0.map { $0.notification } }
    }

    func sendSubmission(hashtagObservable: Observable<String>, messageObservable: Observable<String>, imagesObservable: Observable<[Data]>, videosObservable: Observable<[Data]>) -> Observable<Void> {
        let firstImage = imagesObservable.map { $0.first }
        let firstVideo = videosObservable.map { $0.first }
        let contextSource = contextRepository.observeContext()

        return Observable.combineLatest(
                hashtagObservable,
                messageObservable,
                firstImage,
                firstVideo,
                contextSource
        ) { (hashtag: String, message: String?, image: Data?, video: Data?, context: Context?) in

            // image & video are not sent since there is to multipart support and I cant test submission
            let submission = Submission(
                    hashtag: hashtag,
                    message: message,
                    image: nil,
                    video: nil,
                    //image: image != nil ? File(filename: "user_submission,jpeg", mime: "image/jpeg", data: image!) : nil,
                    //video: video != nil ? File(filename: "user_submission.mp4", mime: "vid", data: video!) : nil,
                    context: context)

            return submission
        }.flatMap { (submission: Submission) in
            self.apiInteractor.postSubmission(submission: submission)
                    .do(onNext: { _ in self.shouldReloadForNewSubmissions.value = true })
        }
    }
}
