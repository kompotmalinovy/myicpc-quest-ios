//
// Created by Vlad Gorbunov on 15/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//
import Foundation
import Locksmith
import AppAuth

/*
    Manager responsible for safe storing auth state for reuse across app relaunch.
    Note, that i'm using a 3rd party generic wrapper.
*/
protocol KeychainManaging {
    func save(authState: OIDAuthState) throws
    func load() -> OIDAuthState?
    func delete() throws
}

class KeychainManager: KeychainManaging {
    fileprivate static let STATE_TAG = "oid_auth_state_tag"
    private let account: String

    init(account: String = Bundle.main.bundleIdentifier ?? "") {
        self.account = account
    }

    func save(authState: OIDAuthState) throws {
        do {
            try Locksmith.saveData(
                   data: [KeychainManager.STATE_TAG: NSKeyedArchiver.archivedData(withRootObject: authState)],
                   forUserAccount: account)
        } catch {
            throw KeychainError.dataSaveFailed
        }
    }

    func load() -> OIDAuthState? {
        let dict = Locksmith.loadDataForUserAccount(userAccount: account)
        if let data = dict?[KeychainManager.STATE_TAG] as? Data {
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? OIDAuthState
        } else {
            return nil
        }
    }

    func delete() throws {
        do {
            try Locksmith.deleteDataForUserAccount(userAccount: account)
        } catch {
            throw KeychainError.dataDeleteFailed
        }
    }
}

enum KeychainError: Error {
    case dataDeleteFailed
    case dataSaveFailed
}