//
// Created by Vlad Gorbunov on 15/03/2018.
// Copyright (c) 2018 Vlad Gorbunov. All rights reserved.
//

import Foundation

private class BundleTokenO {}

public enum Environment {
    
    fileprivate static let plist: [String: Any] = Bundle.main.infoDictionary!
    
    enum Test {
        static var name: String { return plist["test"] as! String }
    }
    
    enum Contest {
        static var code: String { return plist["contestCode"] as! String }
    }

    enum Auth {
        fileprivate static let authDict = plist["auth"] as! [String: Any]

        static var authEndpoint: String { return authDict["AuthEndpoint"] as! String }
        static var tokenEndpoint: String { return authDict["TokenEndpoint"] as! String }
        static var clientId: String { return authDict["ClientId"] as! String }
        static var redirectURL: String { return authDict["RedirectUrl"] as! String }
        static var logoutEndpoint: String { return authDict["LogoutEndpoint"] as! String }
    }

    enum API {
        fileprivate static let apiDict = plist["api"] as! [String: Any]

        static var baseURL: String { return apiDict["BaseUrl"] as! String }
    }

    enum External {
        fileprivate static let linksDict = plist["external"] as! [String: Any]

        static var rulesLink: String { return linksDict["RulesLink"] as! String }
    }

    enum Context {
        static var appVersion: String { return plist["appVersion"] as! String }
    }
}
